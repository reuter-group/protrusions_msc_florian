REMARK        1/2 of bilayer thickness:   15.4                                  
HEADER    TOXIN                                   26-FEB-07   2EFZ              
TITLE     SOLUTION STRUCTURE OF AN M-1 CONOTOXIN WITH A NOVEL                   
TITLE    2 DISULFIDE LINKAGE                                                    
COMPND    MOL_ID: 1;                                                            
COMPND   2 MOLECULE: M CONOTOXIN MR3.4;                                         
COMPND   4 FRAGMENT: RESIDUES 1-16;                                             
COMPND   5 SYNONYM: M-1 CONOTOXIN;                                              
COMPND   6 ENGINEERED: YES                                                      
SOURCE    MOL_ID: 1;                                                            
SOURCE   2 SYNTHETIC: YES;                                                      
KEYWDS    M-CONOTOXIN MR3E                                                      
EXPDTA    SOLUTION NMR                                                          
NUMMDL    20                                                                    
REVDAT   3   24-FEB-09 2EFZ    1       VERSN                                    
REVDAT   2   15-MAY-07 2EFZ    1       JRNL                                     
REVDAT   1   08-MAY-07 2EFZ    0                                                
JRNL        AUTH   W.H.DU,Y.H.HAN,F.J.HUANG,J.LI,C.W.CHI,W.H.FANG               
JRNL        TITL   SOLUTION STRUCTURE OF AN M-1 CONOTOXIN WITH A                
JRNL        TITL 2 NOVEL DISULFIDE LINKAGE                                      
JRNL        REF    FEBS J.                       V. 274  2596 2007              
JRNL        REFN                   ISSN 1742-464X                               
JRNL        PMID   17437523                                                     
JRNL        DOI    10.1111/J.1742-4658.2007.05795.X                             
REMARK   1                                                                      
REMARK   2                                                                      
REMARK   2 RESOLUTION. NOT APPLICABLE.                                          
REMARK   3                                                                      
REMARK   3 REFINEMENT.                                                          
REMARK   3   PROGRAM     : AMBER 5.0                                            
REMARK   3   AUTHORS     : PEARLMAN,CASE,CALDWELL,ROSS,CHEATHAM,FERGUSON,       
REMARK   3                 SEIBEL,SINGH,WEINER,KOLLMAN                          
REMARK   3                                                                      
REMARK   3  OTHER REFINEMENT REMARKS: THE STRUCTURE ARE BASED ON A TOTAL        
REMARK   3  OF 218 CONSTRAINTS, 190 ARE NOE-DERIVED DISTANCE CONSTRAINTS,       
REMARK   3  13 DIHEDRAL CONSTRAINTS, 15 CONSTRAINTS FROM 3 HYDROGEN BONDS       
REMARK   3  AND 3 DISULFIDE BONDS.                                              
REMARK   4                                                                      
REMARK   4 2EFZ COMPLIES WITH FORMAT V. 3.15, 01-DEC-08                         
REMARK 100                                                                      
REMARK 210                                                                      
REMARK 210 EXPERIMENTAL DETAILS                                                 
REMARK 210  EXPERIMENT TYPE                : NMR                                
REMARK 210  TEMPERATURE           (KELVIN) : 294                                
REMARK 210  IONIC STRENGTH                 : NULL                               
REMARK 210  PRESSURE                       : AMBIENT                            
REMARK 210  SAMPLE CONTENTS                : 2.0MM; 0.01% TFA; 90% H2O,         
REMARK 210                                   10% D2O; 2.0MM; 0.01% TFA;         
REMARK 210                                   99.99% D2O                         
REMARK 210                                                                      
REMARK 210  NMR EXPERIMENTS CONDUCTED      : 2D NOESY, 2D TOCSY, DQF-COSY       
REMARK 210  SPECTROMETER FIELD STRENGTH    : 500 MHZ                            
REMARK 210  SPECTROMETER MODEL             : AVANCE                             
REMARK 210  SPECTROMETER MANUFACTURER      : BRUKER                             
REMARK 210                                                                      
REMARK 210  STRUCTURE DETERMINATION.                                            
REMARK 210   SOFTWARE USED                 : XWINNMR 3.5, TOPSPIN 1.3B,         
REMARK 210                                   SPARKY 3.113, DYANA 1.5            
REMARK 210   METHOD USED                   : DISTANCE GEOMETRY, SIMULATED       
REMARK 210                                   ANNEALING, MOLECULAR DYNAMICS      
REMARK 210                                                                      
REMARK 210 CONFORMERS, NUMBER CALCULATED   : 100                                
REMARK 210 CONFORMERS, NUMBER SUBMITTED    : 20                                 
REMARK 210 CONFORMERS, SELECTION CRITERIA  : STRUCTURES WITH THE LOWEST         
REMARK 210                                   ENERGY                             
REMARK 210                                                                      
REMARK 210 BEST REPRESENTATIVE CONFORMER IN THIS ENSEMBLE : 1                   
REMARK 210                                                                      
REMARK 210 REMARK: NULL                                                         
REMARK 215                                                                      
REMARK 215 NMR STUDY                                                            
REMARK 215 NMR DATA.  PROTEIN DATA BANK CONVENTIONS REQUIRE THAT                
REMARK 215 CRYST1 AND SCALE RECORDS BE INCLUDED, BUT THE VALUES ON              
REMARK 500                                                                      
REMARK 500 GEOMETRY AND STEREOCHEMISTRY                                         
REMARK 500 SUBTOPIC: TORSION ANGLES                                             
REMARK 500                                                                      
REMARK 500 TORSION ANGLES OUTSIDE THE EXPECTED RAMACHANDRAN REGIONS:            
REMARK 500 (M=MODEL NUMBER; RES=RESIDUE NAME; C=CHAIN IDENTIFIER;               
REMARK 500 SSEQ=SEQUENCE NUMBER; I=INSERTION CODE).                             
REMARK 500                                                                      
REMARK 500 STANDARD TABLE:                                                      
REMARK 500 FORMAT:(10X,I3,1X,A3,1X,A1,I4,A1,4X,F7.2,3X,F7.2)                    
REMARK 500                                                                      
REMARK 500 EXPECTED VALUES: GJ KLEYWEGT AND TA JONES (1996). PHI/PSI-           
REMARK 500                                                                      
REMARK 500  M RES CSSEQI        PSI       PHI                                   
REMARK 500  1 CYS A  15       94.68     54.68                                   
REMARK 500  2 CYS A  15       90.75     48.09                                   
REMARK 500  3 CYS A  15       91.18     55.82                                   
REMARK 500  4 CYS A  15       91.91     49.04                                   
REMARK 500  5 CYS A  15       91.67     50.22                                   
REMARK 500  6 CYS A  15       81.76     52.53                                   
REMARK 500  7 CYS A  15       85.35     48.18                                   
REMARK 500  8 CYS A  15       83.23     53.53                                   
REMARK 500  9 TYR A  13      -49.16    -28.96                                   
REMARK 500  9 CYS A  15       92.10     49.67                                   
REMARK 500 10 CYS A  15       89.94     48.44                                   
REMARK 500 11 CYS A  15       93.19     56.88                                   
REMARK 500 12 CYS A  15      106.17     57.37                                   
REMARK 500 13 CYS A  15      107.62     57.89                                   
REMARK 500 14 CYS A  15       98.24     57.21                                   
REMARK 500 15 CYS A  15      105.62     57.14                                   
REMARK 500 16 CYS A  15      108.62     58.66                                   
REMARK 500 17 CYS A  15      106.75     57.90                                   
REMARK 500 18 CYS A  15      106.42     58.36                                   
REMARK 500 19 CYS A  15      106.57     58.44                                   
REMARK 500 20 CYS A   8       86.17    -67.61                                   
REMARK 500 20 CYS A  15      100.41     63.68                                   
REMARK 500                                                                      
REMARK 500 REMARK: NULL                                                         
REMARK 500                                                                      
REMARK 500 GEOMETRY AND STEREOCHEMISTRY                                         
REMARK 500 SUBTOPIC: PLANAR GROUPS                                              
REMARK 500                                                                      
REMARK 500 PLANAR GROUPS IN THE FOLLOWING RESIDUES HAVE A TOTAL                 
REMARK 500 RMS DISTANCE OF ALL ATOMS FROM THE BEST-FIT PLANE                    
REMARK 500 BY MORE THAN AN EXPECTED VALUE OF 6*RMSD, WITH AN                    
REMARK 500 RMSD 0.02 ANGSTROMS, OR AT LEAST ONE ATOM HAS                        
REMARK 500 AN RMSD GREATER THAN THIS VALUE                                      
REMARK 500 (M=MODEL NUMBER; RES=RESIDUE NAME; C=CHAIN IDENTIFIER;               
REMARK 500 SSEQ=SEQUENCE NUMBER; I=INSERTION CODE).                             
REMARK 500                                                                      
REMARK 500  M RES CSSEQI        RMS     TYPE                                    
REMARK 500  8 PHE A   5         0.08    SIDE_CHAIN                              
REMARK 500                                                                      
REMARK 500 REMARK: NULL                                                         
REMARK 900                                                                      
REMARK 900 RELATED ENTRIES                                                      
REMARK 900 RELATED ID: 1E76   RELATED DB: PDB                                   
REMARK 900 NMR SOLUTION STRUCTURE OF ALPHA-CONOTOXIN IM1 POINT                  
REMARK 900 MUTATION VARIANT D5N                                                 
REMARK 900 RELATED ID: 1RMK   RELATED DB: PDB                                   
REMARK 900 SOLUTION STRUCTURE OF CONOTOXIN MRVIB                                
REMARK 900 RELATED ID: 1GIB   RELATED DB: PDB                                   
REMARK 900 SOLUTION STRUCTURE OF MU-CONOTOXIN GIIIB                             
REMARK 900 RELATED ID: 1KCP   RELATED DB: PDB                                   
REMARK 900 3D STRUCTURE OF K-CONOTOXIN PVIIA, A NOVEL POTASSIUM                 
DBREF  2EFZ A    1    16  UNP    Q5EHP3   Q5EHP3_CONMR    55     70             
SEQRES   1 A   16  VAL CYS CYS PRO PHE GLY GLY CYS HIS GLU LEU CYS TYR          
SEQRES   2 A   16  CYS CYS ASP                                                  
SSBOND   1 CYS A    2    CYS A   14                          1555   1555  2.09  
SSBOND   2 CYS A    3    CYS A   12                          1555   1555  2.08  
SSBOND   3 CYS A    8    CYS A   15                          1555   1555  2.08  
CRYST1    1.000    1.000    1.000  90.00  90.00  90.00 P 1           1          
ORIGX1      1.000000  0.000000  0.000000        0.00000                         
ORIGX2      0.000000  1.000000  0.000000        0.00000                         
ORIGX3      0.000000  0.000000  1.000000        0.00000                         
SCALE1      1.000000  0.000000  0.000000        0.00000                         
SCALE2      0.000000  1.000000  0.000000        0.00000                         
SCALE3      0.000000  0.000000  1.000000        0.00000                         
ATOM      1  N   VAL A   1      -8.112   3.912  15.292  1.00  0.00           N  
ATOM      2  CA  VAL A   1      -8.311   2.920  16.333  1.00  0.00           C  
ATOM      3  C   VAL A   1      -6.988   2.725  17.077  1.00  0.00           C  
ATOM      4  O   VAL A   1      -5.957   3.243  16.647  1.00  0.00           O  
ATOM      5  CB  VAL A   1      -8.849   1.606  15.737  1.00  0.00           C  
ATOM      6  CG1 VAL A   1     -10.241   1.818  15.130  1.00  0.00           C  
ATOM      7  CG2 VAL A   1      -7.909   1.016  14.674  1.00  0.00           C  
ATOM     17  N   CYS A   2      -7.017   2.001  18.198  1.00  0.00           N  
ATOM     18  CA  CYS A   2      -5.806   1.678  18.942  1.00  0.00           C  
ATOM     19  C   CYS A   2      -5.033   0.580  18.217  1.00  0.00           C  
ATOM     20  O   CYS A   2      -5.595  -0.110  17.367  1.00  0.00           O  
ATOM     21  CB  CYS A   2      -6.164   1.211  20.354  1.00  0.00           C  
ATOM     22  SG  CYS A   2      -4.917   1.560  21.605  1.00  0.00           S  
ATOM     26  N   CYS A   3      -3.764   0.374  18.571  1.00  0.00           N  
ATOM     27  CA  CYS A   3      -3.001  -0.745  18.040  1.00  0.00           C  
ATOM     28  C   CYS A   3      -3.192  -1.909  19.005  1.00  0.00           C  
ATOM     29  O   CYS A   3      -2.826  -1.731  20.164  1.00  0.00           O  
ATOM     30  CB  CYS A   3      -1.521  -0.392  17.928  1.00  0.00           C  
ATOM     31  SG  CYS A   3      -1.145   0.856  16.681  1.00  0.00           S  
ATOM     35  N   PRO A   4      -3.767  -3.051  18.583  1.00  0.00           N  
ATOM     36  CA  PRO A   4      -4.074  -4.169  19.469  1.00  0.00           C  
ATOM     37  C   PRO A   4      -2.792  -4.782  20.036  1.00  0.00           C  
ATOM     38  O   PRO A   4      -1.717  -4.517  19.503  1.00  0.00           O  
ATOM     39  CB  PRO A   4      -4.828  -5.182  18.604  1.00  0.00           C  
ATOM     40  CG  PRO A   4      -4.329  -4.890  17.190  1.00  0.00           C  
ATOM     41  CD  PRO A   4      -4.113  -3.378  17.208  1.00  0.00           C  
ATOM     46  N   PHE A   5      -2.895  -5.635  21.068  1.00  0.00           N  
ATOM     47  CA  PHE A   5      -1.765  -6.426  21.575  1.00  0.00           C  
ATOM     48  C   PHE A   5      -0.889  -6.936  20.425  1.00  0.00           C  
ATOM     49  O   PHE A   5       0.311  -6.671  20.382  1.00  0.00           O  
ATOM     50  CB  PHE A   5      -2.249  -7.640  22.390  1.00  0.00           C  
ATOM     51  CG  PHE A   5      -2.960  -7.412  23.711  1.00  0.00           C  
ATOM     52  CD1 PHE A   5      -2.907  -6.176  24.383  1.00  0.00           C  
ATOM     53  CD2 PHE A   5      -3.625  -8.498  24.314  1.00  0.00           C  
ATOM     54  CE1 PHE A   5      -3.576  -6.011  25.609  1.00  0.00           C  
ATOM     55  CE2 PHE A   5      -4.271  -8.340  25.552  1.00  0.00           C  
ATOM     56  CZ  PHE A   5      -4.257  -7.091  26.195  1.00  0.00           C  
ATOM     65  N   GLY A   6      -1.497  -7.645  19.470  1.00  0.00           N  
ATOM     66  CA  GLY A   6      -0.815  -8.106  18.271  1.00  0.00           C  
ATOM     67  C   GLY A   6      -0.684  -6.969  17.259  1.00  0.00           C  
ATOM     68  O   GLY A   6      -1.203  -7.067  16.150  1.00  0.00           O  
ATOM     72  N   GLY A   7      -0.007  -5.883  17.639  1.00  0.00           N  
ATOM     73  CA  GLY A   7       0.108  -4.700  16.802  1.00  0.00           C  
ATOM     74  C   GLY A   7       0.989  -3.628  17.436  1.00  0.00           C  
ATOM     75  O   GLY A   7       1.875  -3.088  16.774  1.00  0.00           O  
ATOM     79  N   CYS A   8       0.737  -3.290  18.703  1.00  0.00           N  
ATOM     80  CA  CYS A   8       1.431  -2.203  19.381  1.00  0.00           C  
ATOM     81  C   CYS A   8       2.949  -2.392  19.353  1.00  0.00           C  
ATOM     82  O   CYS A   8       3.458  -3.453  19.704  1.00  0.00           O  
ATOM     83  CB  CYS A   8       0.952  -2.034  20.824  1.00  0.00           C  
ATOM     84  SG  CYS A   8       1.997  -0.845  21.705  1.00  0.00           S  
ATOM     88  N   HIS A   9       3.657  -1.340  18.942  1.00  0.00           N  
ATOM     89  CA  HIS A   9       5.083  -1.153  19.104  1.00  0.00           C  
ATOM     90  C   HIS A   9       5.236   0.295  19.570  1.00  0.00           C  
ATOM     91  O   HIS A   9       4.247   1.030  19.595  1.00  0.00           O  
ATOM     92  CB  HIS A   9       5.779  -1.380  17.757  1.00  0.00           C  
ATOM     93  CG  HIS A   9       5.750  -2.816  17.298  1.00  0.00           C  
ATOM     94  ND1 HIS A   9       4.650  -3.499  16.830  1.00  0.00           N  
ATOM     95  CD2 HIS A   9       6.804  -3.692  17.302  1.00  0.00           C  
ATOM     96  CE1 HIS A   9       5.035  -4.758  16.566  1.00  0.00           C  
ATOM     97  NE2 HIS A   9       6.342  -4.923  16.830  1.00  0.00           N  
ATOM    104  N   GLU A  10       6.459   0.718  19.894  1.00  0.00           N  
ATOM    105  CA  GLU A  10       6.762   2.056  20.395  1.00  0.00           C  
ATOM    106  C   GLU A  10       6.172   3.162  19.515  1.00  0.00           C  
ATOM    107  O   GLU A  10       5.797   4.227  19.998  1.00  0.00           O  
ATOM    108  CB  GLU A  10       8.285   2.234  20.486  1.00  0.00           C  
ATOM    109  CG  GLU A  10       8.991   1.082  21.210  1.00  0.00           C  
ATOM    110  CD  GLU A  10      10.467   1.398  21.400  1.00  0.00           C  
ATOM    111  OE1 GLU A  10      11.154   1.486  20.359  1.00  0.00           O  
ATOM    112  OE2 GLU A  10      10.872   1.557  22.570  1.00  0.00           O  
ATOM    117  N   LEU A  11       6.103   2.897  18.211  1.00  0.00           N  
ATOM    118  CA  LEU A  11       5.587   3.796  17.203  1.00  0.00           C  
ATOM    119  C   LEU A  11       4.158   4.262  17.510  1.00  0.00           C  
ATOM    120  O   LEU A  11       3.775   5.364  17.127  1.00  0.00           O  
ATOM    121  CB  LEU A  11       5.628   3.040  15.872  1.00  0.00           C  
ATOM    122  CG  LEU A  11       7.061   2.864  15.347  1.00  0.00           C  
ATOM    123  CD1 LEU A  11       7.125   1.661  14.398  1.00  0.00           C  
ATOM    124  CD2 LEU A  11       7.540   4.120  14.608  1.00  0.00           C  
ATOM    135  N   CYS A  12       3.348   3.402  18.135  1.00  0.00           N  
ATOM    136  CA  CYS A  12       1.911   3.598  18.245  1.00  0.00           C  
ATOM    137  C   CYS A  12       1.527   4.123  19.622  1.00  0.00           C  
ATOM    138  O   CYS A  12       1.515   3.374  20.594  1.00  0.00           O  
ATOM    139  CB  CYS A  12       1.183   2.296  17.925  1.00  0.00           C  
ATOM    140  SG  CYS A  12      -0.605   2.518  17.803  1.00  0.00           S  
ATOM    144  N   TYR A  13       1.192   5.413  19.694  1.00  0.00           N  
ATOM    145  CA  TYR A  13       0.870   6.134  20.902  1.00  0.00           C  
ATOM    146  C   TYR A  13      -0.193   5.428  21.749  1.00  0.00           C  
ATOM    147  O   TYR A  13      -0.043   5.329  22.963  1.00  0.00           O  
ATOM    148  CB  TYR A  13       0.402   7.525  20.460  1.00  0.00           C  
ATOM    149  CG  TYR A  13       1.478   8.593  20.471  1.00  0.00           C  
ATOM    150  CD1 TYR A  13       2.474   8.609  19.475  1.00  0.00           C  
ATOM    151  CD2 TYR A  13       1.474   9.586  21.470  1.00  0.00           C  
ATOM    152  CE1 TYR A  13       3.449   9.621  19.470  1.00  0.00           C  
ATOM    153  CE2 TYR A  13       2.447  10.599  21.459  1.00  0.00           C  
ATOM    154  CZ  TYR A  13       3.434  10.618  20.461  1.00  0.00           C  
ATOM    155  OH  TYR A  13       4.367  11.611  20.455  1.00  0.00           O  
ATOM    164  N   CYS A  14      -1.295   4.995  21.124  1.00  0.00           N  
ATOM    165  CA  CYS A  14      -2.450   4.494  21.863  1.00  0.00           C  
ATOM    166  C   CYS A  14      -2.128   3.207  22.625  1.00  0.00           C  
ATOM    167  O   CYS A  14      -2.288   3.152  23.838  1.00  0.00           O  
ATOM    168  CB  CYS A  14      -3.652   4.309  20.936  1.00  0.00           C  
ATOM    169  SG  CYS A  14      -5.079   3.629  21.812  1.00  0.00           S  
ATOM    173  N   CYS A  15      -1.676   2.188  21.888  1.00  0.00           N  
ATOM    174  CA  CYS A  15      -1.158   0.924  22.400  1.00  0.00           C  
ATOM    175  C   CYS A  15      -2.104   0.176  23.349  1.00  0.00           C  
ATOM    176  O   CYS A  15      -2.109   0.397  24.559  1.00  0.00           O  
ATOM    177  CB  CYS A  15       0.188   1.152  23.081  1.00  0.00           C  
ATOM    178  SG  CYS A  15       1.030  -0.391  23.490  1.00  0.00           S  
ATOM    182  N   ASP A  16      -2.885  -0.758  22.803  1.00  0.00           N  
ATOM    183  CA  ASP A  16      -3.739  -1.663  23.566  1.00  0.00           C  
ATOM    184  C   ASP A  16      -2.961  -2.342  24.697  1.00  0.00           C  
ATOM    185  O   ASP A  16      -1.870  -2.869  24.475  1.00  0.00           O  
ATOM    186  CB  ASP A  16      -4.283  -2.703  22.589  1.00  0.00           C  
ATOM    187  CG  ASP A  16      -5.005  -3.867  23.239  1.00  0.00           C  
ATOM    188  OD1 ASP A  16      -5.441  -3.692  24.396  1.00  0.00           O  
ATOM    189  OD2 ASP A  16      -5.078  -4.913  22.552  1.00  0.00           O  
HETATM 2712  O   DUM  2712     -12.000  -2.000  15.400                          
HETATM 2713  O   DUM  2713     -12.000   0.000  15.400                          
HETATM 2714  O   DUM  2714     -12.000   2.000  15.400                          
HETATM 2791  O   DUM  2791     -10.000  -6.000  15.400                          
HETATM 2792  O   DUM  2792     -10.000  -4.000  15.400                          
HETATM 2793  O   DUM  2793     -10.000  -2.000  15.400                          
HETATM 2794  O   DUM  2794     -10.000   0.000  15.400                          
HETATM 2795  O   DUM  2795     -10.000   2.000  15.400                          
HETATM 2796  O   DUM  2796     -10.000   4.000  15.400                          
HETATM 2797  O   DUM  2797     -10.000   6.000  15.400                          
HETATM 2871  O   DUM  2871      -8.000  -8.000  15.400                          
HETATM 2872  O   DUM  2872      -8.000  -6.000  15.400                          
HETATM 2873  O   DUM  2873      -8.000  -4.000  15.400                          
HETATM 2874  O   DUM  2874      -8.000  -2.000  15.400                          
HETATM 2875  O   DUM  2875      -8.000   0.000  15.400                          
HETATM 2876  O   DUM  2876      -8.000   2.000  15.400                          
HETATM 2877  O   DUM  2877      -8.000   4.000  15.400                          
HETATM 2878  O   DUM  2878      -8.000   6.000  15.400                          
HETATM 2879  O   DUM  2879      -8.000   8.000  15.400                          
HETATM 2951  O   DUM  2951      -6.000 -10.000  15.400                          
HETATM 2952  O   DUM  2952      -6.000  -8.000  15.400                          
HETATM 2953  O   DUM  2953      -6.000  -6.000  15.400                          
HETATM 2954  O   DUM  2954      -6.000  -4.000  15.400                          
HETATM 2955  O   DUM  2955      -6.000  -2.000  15.400                          
HETATM 2956  O   DUM  2956      -6.000   0.000  15.400                          
HETATM 2957  O   DUM  2957      -6.000   2.000  15.400                          
HETATM 2958  O   DUM  2958      -6.000   4.000  15.400                          
HETATM 2959  O   DUM  2959      -6.000   6.000  15.400                          
HETATM 2960  O   DUM  2960      -6.000   8.000  15.400                          
HETATM 2961  O   DUM  2961      -6.000  10.000  15.400                          
HETATM 3032  O   DUM  3032      -4.000 -10.000  15.400                          
HETATM 3033  O   DUM  3033      -4.000  -8.000  15.400                          
HETATM 3034  O   DUM  3034      -4.000  -6.000  15.400                          
HETATM 3035  O   DUM  3035      -4.000  -4.000  15.400                          
HETATM 3036  O   DUM  3036      -4.000  -2.000  15.400                          
HETATM 3037  O   DUM  3037      -4.000   0.000  15.400                          
HETATM 3038  O   DUM  3038      -4.000   2.000  15.400                          
HETATM 3039  O   DUM  3039      -4.000   4.000  15.400                          
HETATM 3040  O   DUM  3040      -4.000   6.000  15.400                          
HETATM 3041  O   DUM  3041      -4.000   8.000  15.400                          
HETATM 3042  O   DUM  3042      -4.000  10.000  15.400                          
HETATM 3112  O   DUM  3112      -2.000 -12.000  15.400                          
HETATM 3113  O   DUM  3113      -2.000 -10.000  15.400                          
HETATM 3114  O   DUM  3114      -2.000  -8.000  15.400                          
HETATM 3115  O   DUM  3115      -2.000  -6.000  15.400                          
HETATM 3116  O   DUM  3116      -2.000  -4.000  15.400                          
HETATM 3117  O   DUM  3117      -2.000  -2.000  15.400                          
HETATM 3118  O   DUM  3118      -2.000   0.000  15.400                          
HETATM 3119  O   DUM  3119      -2.000   2.000  15.400                          
HETATM 3120  O   DUM  3120      -2.000   4.000  15.400                          
HETATM 3121  O   DUM  3121      -2.000   6.000  15.400                          
HETATM 3122  O   DUM  3122      -2.000   8.000  15.400                          
HETATM 3123  O   DUM  3123      -2.000  10.000  15.400                          
HETATM 3124  O   DUM  3124      -2.000  12.000  15.400                          
HETATM 3193  O   DUM  3193       0.000 -12.000  15.400                          
HETATM 3194  O   DUM  3194       0.000 -10.000  15.400                          
HETATM 3195  O   DUM  3195       0.000  -8.000  15.400                          
HETATM 3196  O   DUM  3196       0.000  -6.000  15.400                          
HETATM 3197  O   DUM  3197       0.000  -4.000  15.400                          
HETATM 3198  O   DUM  3198       0.000  -2.000  15.400                          
HETATM 3199  O   DUM  3199       0.000   0.000  15.400                          
HETATM 3200  O   DUM  3200       0.000   2.000  15.400                          
HETATM 3201  O   DUM  3201       0.000   4.000  15.400                          
HETATM 3202  O   DUM  3202       0.000   6.000  15.400                          
HETATM 3203  O   DUM  3203       0.000   8.000  15.400                          
HETATM 3204  O   DUM  3204       0.000  10.000  15.400                          
HETATM 3205  O   DUM  3205       0.000  12.000  15.400                          
HETATM 3274  O   DUM  3274       2.000 -12.000  15.400                          
HETATM 3275  O   DUM  3275       2.000 -10.000  15.400                          
HETATM 3276  O   DUM  3276       2.000  -8.000  15.400                          
HETATM 3277  O   DUM  3277       2.000  -6.000  15.400                          
HETATM 3278  O   DUM  3278       2.000  -4.000  15.400                          
HETATM 3279  O   DUM  3279       2.000  -2.000  15.400                          
HETATM 3280  O   DUM  3280       2.000   0.000  15.400                          
HETATM 3281  O   DUM  3281       2.000   2.000  15.400                          
HETATM 3282  O   DUM  3282       2.000   4.000  15.400                          
HETATM 3283  O   DUM  3283       2.000   6.000  15.400                          
HETATM 3284  O   DUM  3284       2.000   8.000  15.400                          
HETATM 3285  O   DUM  3285       2.000  10.000  15.400                          
HETATM 3286  O   DUM  3286       2.000  12.000  15.400                          
HETATM 3356  O   DUM  3356       4.000 -10.000  15.400                          
HETATM 3357  O   DUM  3357       4.000  -8.000  15.400                          
HETATM 3358  O   DUM  3358       4.000  -6.000  15.400                          
HETATM 3359  O   DUM  3359       4.000  -4.000  15.400                          
HETATM 3360  O   DUM  3360       4.000  -2.000  15.400                          
HETATM 3361  O   DUM  3361       4.000   0.000  15.400                          
HETATM 3362  O   DUM  3362       4.000   2.000  15.400                          
HETATM 3363  O   DUM  3363       4.000   4.000  15.400                          
HETATM 3364  O   DUM  3364       4.000   6.000  15.400                          
HETATM 3365  O   DUM  3365       4.000   8.000  15.400                          
HETATM 3366  O   DUM  3366       4.000  10.000  15.400                          
HETATM 3437  O   DUM  3437       6.000 -10.000  15.400                          
HETATM 3438  O   DUM  3438       6.000  -8.000  15.400                          
HETATM 3439  O   DUM  3439       6.000  -6.000  15.400                          
HETATM 3440  O   DUM  3440       6.000  -4.000  15.400                          
HETATM 3441  O   DUM  3441       6.000  -2.000  15.400                          
HETATM 3442  O   DUM  3442       6.000   0.000  15.400                          
HETATM 3443  O   DUM  3443       6.000   2.000  15.400                          
HETATM 3444  O   DUM  3444       6.000   4.000  15.400                          
HETATM 3445  O   DUM  3445       6.000   6.000  15.400                          
HETATM 3446  O   DUM  3446       6.000   8.000  15.400                          
HETATM 3447  O   DUM  3447       6.000  10.000  15.400                          
HETATM 3519  O   DUM  3519       8.000  -8.000  15.400                          
HETATM 3520  O   DUM  3520       8.000  -6.000  15.400                          
HETATM 3521  O   DUM  3521       8.000  -4.000  15.400                          
HETATM 3522  O   DUM  3522       8.000  -2.000  15.400                          
HETATM 3523  O   DUM  3523       8.000   0.000  15.400                          
HETATM 3524  O   DUM  3524       8.000   2.000  15.400                          
HETATM 3525  O   DUM  3525       8.000   4.000  15.400                          
HETATM 3526  O   DUM  3526       8.000   6.000  15.400                          
HETATM 3527  O   DUM  3527       8.000   8.000  15.400                          
HETATM 3601  O   DUM  3601      10.000  -6.000  15.400                          
HETATM 3602  O   DUM  3602      10.000  -4.000  15.400                          
HETATM 3603  O   DUM  3603      10.000  -2.000  15.400                          
HETATM 3604  O   DUM  3604      10.000   0.000  15.400                          
HETATM 3605  O   DUM  3605      10.000   2.000  15.400                          
HETATM 3606  O   DUM  3606      10.000   4.000  15.400                          
HETATM 3607  O   DUM  3607      10.000   6.000  15.400                          
HETATM 3684  O   DUM  3684      12.000  -2.000  15.400                          
HETATM 3685  O   DUM  3685      12.000   0.000  15.400                          
HETATM 3686  O   DUM  3686      12.000   2.000  15.400                          
