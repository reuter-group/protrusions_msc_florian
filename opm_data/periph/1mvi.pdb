REMARK        1/2 of bilayer thickness:   15.4                                  
HEADER    NEUROTOXIN                              02-AUG-96   1MVI              
TITLE     N-TYPE CALCIUM CHANNEL BLOCKER, OMEGA-CONOTOXIN MVIIA, NMR,           
TITLE    2 15 STRUCTURES                                                        
COMPND    MOL_ID: 1;                                                            
COMPND   2 MOLECULE: MVIIA;                                                     
COMPND   4 SYNONYM: OMEGA-CONOTOXIN MVIIA;                                      
COMPND   5 ENGINEERED: YES                                                      
SOURCE    MOL_ID: 1;                                                            
SOURCE   2 ORGANISM_SCIENTIFIC: CONUS MAGUS;                                    
SOURCE   3 ORGANISM_COMMON: MAGUS CONE;                                         
SOURCE   4 ORGANISM_TAXID: 6492                                                 
KEYWDS    CONUS MAGUS PEPTIDE SPECIFIC TO N-TYPE VOLTAGE SENSITIVE              
KEYWDS   2 CALCIUM CHANNEL, NEUROTOXIN                                          
EXPDTA    SOLUTION NMR                                                          
NUMMDL    15                                                                    
AUTHOR    K.J.NIELSEN,L.THOMAS,R.J.LEWIS,P.F.ALEWOOD,D.J.CRAIK                  
REVDAT   2   24-FEB-09 1MVI    1       VERSN                                    
REVDAT   1   12-AUG-97 1MVI    0                                                
JRNL        AUTH   K.J.NIELSEN,L.THOMAS,R.J.LEWIS,P.F.ALEWOOD,                  
JRNL        AUTH 2 D.J.CRAIK                                                    
JRNL        TITL   A CONSENSUS STRUCTURE FOR OMEGA-CONOTOXINS WITH              
JRNL        TITL 2 DIFFERENT SELECTIVITIES FOR VOLTAGE-SENSITIVE                
JRNL        TITL 3 CALCIUM CHANNEL SUBTYPES: COMPARISON OF MVIIA,               
JRNL        TITL 4 SVIB AND SNX-202.                                            
JRNL        REF    J.MOL.BIOL.                   V. 263   297 1996              
JRNL        REFN                   ISSN 0022-2836                               
JRNL        PMID   8913308                                                      
JRNL        DOI    10.1006/JMBI.1996.0576                                       
REMARK   1                                                                      
REMARK   2                                                                      
REMARK   2 RESOLUTION. NOT APPLICABLE.                                          
REMARK   3                                                                      
REMARK   3 REFINEMENT.                                                          
REMARK   3   PROGRAM     : X-PLOR 3.1                                           
REMARK   3   AUTHORS     : BRUNGER                                              
REMARK   3                                                                      
REMARK   3  OTHER REFINEMENT REMARKS: A TOTAL OF 50 INITIAL STRUCTURES          
REMARK   3  WERE CALCULATED USING A SIMULATED ANNEALING PROGRAM AS              
REMARK   3  SUPPLIED WITH X-PLOR 3.1. THE FINAL 20 STRUCTURES WITH THE          
REMARK   3  LOWEST OVERALL ENERGIES AND FEWEST VIOLATIONS OF NOE AND            
REMARK   3  DIHEDRAL RESTRAINTS WERE ENERGY MINIMIZED IN X-PLOR USING THE       
REMARK   3  4 187-217.]                                                         
REMARK   4                                                                      
REMARK   4 1MVI COMPLIES WITH FORMAT V. 3.15, 01-DEC-08                         
REMARK 100                                                                      
REMARK 210                                                                      
REMARK 210 EXPERIMENTAL DETAILS                                                 
REMARK 210  EXPERIMENT TYPE                : NMR                                
REMARK 210  TEMPERATURE           (KELVIN) : 293                                
REMARK 210  IONIC STRENGTH                 : NULL                               
REMARK 210  PRESSURE                       : NULL                               
REMARK 210  SAMPLE CONTENTS                : NULL                               
REMARK 210                                                                      
REMARK 210  NMR EXPERIMENTS CONDUCTED      : NOESY, TOCSY, DQF-COSY, E-COSY     
REMARK 210  SPECTROMETER FIELD STRENGTH    : 500 MHZ                            
REMARK 210  SPECTROMETER MODEL             : ARX                                
REMARK 210  SPECTROMETER MANUFACTURER      : BRUKER                             
REMARK 210                                                                      
REMARK 210  STRUCTURE DETERMINATION.                                            
REMARK 210   SOFTWARE USED                 : X-PLOR                             
REMARK 210   METHOD USED                   : MD/SIMULATED ANNEALING             
REMARK 210                                                                      
REMARK 210 CONFORMERS, NUMBER CALCULATED   : 50                                 
REMARK 210 CONFORMERS, NUMBER SUBMITTED    : 15                                 
REMARK 210 CONFORMERS, SELECTION CRITERIA  : LOW E AND FEW VIOLATIONS OF        
REMARK 210                                   EXPERIMENTAL RESTRAINTS            
REMARK 210                                                                      
REMARK 210 BEST REPRESENTATIVE CONFORMER IN THIS ENSEMBLE : NULL                
REMARK 210                                                                      
REMARK 210 REMARK: NULL                                                         
REMARK 215                                                                      
REMARK 215 NMR STUDY                                                            
REMARK 215 NMR DATA.  PROTEIN DATA BANK CONVENTIONS REQUIRE THAT                
REMARK 215 CRYST1 AND SCALE RECORDS BE INCLUDED, BUT THE VALUES ON              
REMARK 500                                                                      
REMARK 500 GEOMETRY AND STEREOCHEMISTRY                                         
REMARK 500 SUBTOPIC: TORSION ANGLES                                             
REMARK 500                                                                      
REMARK 500 TORSION ANGLES OUTSIDE THE EXPECTED RAMACHANDRAN REGIONS:            
REMARK 500 (M=MODEL NUMBER; RES=RESIDUE NAME; C=CHAIN IDENTIFIER;               
REMARK 500 SSEQ=SEQUENCE NUMBER; I=INSERTION CODE).                             
REMARK 500                                                                      
REMARK 500 STANDARD TABLE:                                                      
REMARK 500 FORMAT:(10X,I3,1X,A3,1X,A1,I4,A1,4X,F7.2,3X,F7.2)                    
REMARK 500                                                                      
REMARK 500 EXPECTED VALUES: GJ KLEYWEGT AND TA JONES (1996). PHI/PSI-           
REMARK 500                                                                      
REMARK 500  M RES CSSEQI        PSI       PHI                                   
REMARK 500  1 LYS A   4       93.76    -65.97                                   
REMARK 500  1 CYS A   8     -157.96    -80.23                                   
REMARK 500  1 ARG A  21       70.62   -118.93                                   
REMARK 500  1 SER A  22       29.05     49.68                                   
REMARK 500  2 CYS A  20      120.51    -26.63                                   
REMARK 500  3 CYS A  20      120.51    -26.63                                   
REMARK 500  4 CYS A   8     -159.94    -79.91                                   
REMARK 500  4 CYS A  20      130.01    -31.86                                   
REMARK 500  5 LYS A   7       91.07    -65.74                                   
REMARK 500  5 CYS A  20      133.04    -28.58                                   
REMARK 500  6 MET A  12       42.37   -144.36                                   
REMARK 500  6 TYR A  13       66.94     -8.35                                   
REMARK 500  6 CYS A  20      125.56    -21.10                                   
REMARK 500  6 ARG A  21       66.72   -113.85                                   
REMARK 500  7 MET A  12       38.42   -163.74                                   
REMARK 500  7 TYR A  13       63.88     -4.74                                   
REMARK 500  7 CYS A  20      120.62    -35.40                                   
REMARK 500  7 ARG A  21       65.89   -117.02                                   
REMARK 500  8 CYS A  20      130.42    -28.68                                   
REMARK 500  9 LYS A   7       90.79    -63.87                                   
REMARK 500  9 MET A  12       38.57   -156.87                                   
REMARK 500  9 TYR A  13       64.39     -5.35                                   
REMARK 500  9 CYS A  20      113.29    -25.08                                   
REMARK 500  9 ARG A  21       67.77   -111.42                                   
REMARK 500 10 CYS A   8     -157.69    -81.22                                   
REMARK 500 10 CYS A  20      122.55    -33.17                                   
REMARK 500 10 ARG A  21       66.10   -117.27                                   
REMARK 500 11 LYS A   4       93.76    -65.97                                   
REMARK 500 11 CYS A   8     -157.96    -80.23                                   
REMARK 500 11 ARG A  21       70.62   -118.93                                   
REMARK 500 11 SER A  22       29.05     49.68                                   
REMARK 500 12 LYS A   7       94.17    -64.28                                   
REMARK 500 12 MET A  12       50.89   -164.52                                   
REMARK 500 12 TYR A  13       66.28     -7.33                                   
REMARK 500 12 CYS A  20      120.96    -31.03                                   
REMARK 500 12 ARG A  21       69.83   -119.09                                   
REMARK 500 13 CYS A   8     -161.53   -102.26                                   
REMARK 500 13 CYS A  20      121.32    -26.87                                   
REMARK 500 13 ARG A  21       65.31   -119.59                                   
REMARK 500 13 SER A  22       27.75     49.93                                   
REMARK 500 14 CYS A   8     -157.79    -81.29                                   
REMARK 500 14 CYS A  20      132.87    -35.83                                   
REMARK 500 15 CYS A   8     -159.78    -79.79                                   
REMARK 500 15 CYS A  20      120.58    -28.25                                   
REMARK 500 15 ARG A  21       67.66   -116.35                                   
REMARK 500 15 SER A  22       28.88     49.81                                   
REMARK 500                                                                      
REMARK 500 REMARK: NULL                                                         
REMARK 800                                                                      
REMARK 800 SITE                                                                 
REMARK 800 SITE_IDENTIFIER: AC1                                                 
REMARK 800 EVIDENCE_CODE: SOFTWARE                                              
REMARK 800 SITE_DESCRIPTION: BINDING SITE FOR RESIDUE NH2 A 26                  
DBREF  1MVI A    1    25  UNP    P05484   CXO7A_CONMA      1     25             
SEQRES   1 A   26  CYS LYS GLY LYS GLY ALA LYS CYS SER ARG LEU MET TYR          
SEQRES   2 A   26  ASP CYS CYS THR GLY SER CYS ARG SER GLY LYS CYS NH2          
HET    NH2  A  26       3                                                       
SHEET    1  S1 3 CYS A  20  ARG A  21  0                                        
SHEET    2  S1 3 GLY A  23  CYS A  25 -1  O  LYS A  24   N  ARG A  21           
SHEET    3  S1 3 ALA A   6  CYS A   8 -1  N  CYS A   8   O  GLY A  23           
SSBOND   1 CYS A    1    CYS A   16                          1555   1555  2.02  
SSBOND   2 CYS A    8    CYS A   20                          1555   1555  2.02  
SSBOND   3 CYS A   15    CYS A   25                          1555   1555  2.02  
LINK         N   NH2 A  26                 C   CYS A  25     1555   1555  1.30  
SITE     1 AC1  2 SER A  19  CYS A  25                                          
CRYST1    1.000    1.000    1.000  90.00  90.00  90.00 P 1           1          
ORIGX1      1.000000  0.000000  0.000000        0.00000                         
ORIGX2      0.000000  1.000000  0.000000        0.00000                         
ORIGX3      0.000000  0.000000  1.000000        0.00000                         
SCALE1      1.000000  0.000000  0.000000        0.00000                         
SCALE2      0.000000  1.000000  0.000000        0.00000                         
SCALE3      0.000000  0.000000  1.000000        0.00000                         
ATOM      1  N   CYS A   1       8.849   2.859  16.226  1.00  0.00           N  
ATOM      2  CA  CYS A   1       7.767   2.059  16.867  1.00  0.00           C  
ATOM      3  C   CYS A   1       7.308   2.746  18.160  1.00  0.00           C  
ATOM      4  O   CYS A   1       7.609   3.900  18.397  1.00  0.00           O  
ATOM      5  CB  CYS A   1       8.279   0.650  17.216  1.00  0.00           C  
ATOM      6  SG  CYS A   1       9.019   0.435  18.856  1.00  0.00           S  
ATOM     13  N   LYS A   2       6.581   1.994  18.949  1.00  0.00           N  
ATOM     14  CA  LYS A   2       6.056   2.504  20.249  1.00  0.00           C  
ATOM     15  C   LYS A   2       6.363   1.501  21.378  1.00  0.00           C  
ATOM     16  O   LYS A   2       6.143   0.312  21.267  1.00  0.00           O  
ATOM     17  CB  LYS A   2       4.533   2.730  20.098  1.00  0.00           C  
ATOM     18  CG  LYS A   2       4.286   4.183  19.618  1.00  0.00           C  
ATOM     19  CD  LYS A   2       2.771   4.518  19.646  1.00  0.00           C  
ATOM     20  CE  LYS A   2       2.538   5.995  20.026  1.00  0.00           C  
ATOM     21  NZ  LYS A   2       1.487   6.579  19.144  1.00  0.00           N  
ATOM     35  N   GLY A   3       6.885   2.040  22.447  1.00  0.00           N  
ATOM     36  CA  GLY A   3       7.247   1.224  23.646  1.00  0.00           C  
ATOM     37  C   GLY A   3       5.989   0.797  24.411  1.00  0.00           C  
ATOM     38  O   GLY A   3       4.950   1.418  24.290  1.00  0.00           O  
ATOM     42  N   LYS A   4       6.131  -0.255  25.178  1.00  0.00           N  
ATOM     43  CA  LYS A   4       4.990  -0.790  25.984  1.00  0.00           C  
ATOM     44  C   LYS A   4       4.573   0.218  27.070  1.00  0.00           C  
ATOM     45  O   LYS A   4       5.114   0.250  28.158  1.00  0.00           O  
ATOM     46  CB  LYS A   4       5.429  -2.133  26.617  1.00  0.00           C  
ATOM     47  CG  LYS A   4       5.490  -3.238  25.523  1.00  0.00           C  
ATOM     48  CD  LYS A   4       6.945  -3.448  25.023  1.00  0.00           C  
ATOM     49  CE  LYS A   4       7.622  -4.590  25.805  1.00  0.00           C  
ATOM     50  NZ  LYS A   4       7.907  -4.159  27.204  1.00  0.00           N  
ATOM     64  N   GLY A   5       3.602   1.011  26.698  1.00  0.00           N  
ATOM     65  CA  GLY A   5       3.025   2.073  27.577  1.00  0.00           C  
ATOM     66  C   GLY A   5       2.825   3.346  26.741  1.00  0.00           C  
ATOM     67  O   GLY A   5       2.674   4.426  27.281  1.00  0.00           O  
ATOM     71  N   ALA A   6       2.835   3.166  25.442  1.00  0.00           N  
ATOM     72  CA  ALA A   6       2.658   4.295  24.478  1.00  0.00           C  
ATOM     73  C   ALA A   6       1.202   4.398  24.020  1.00  0.00           C  
ATOM     74  O   ALA A   6       0.461   3.438  24.096  1.00  0.00           O  
ATOM     75  CB  ALA A   6       3.569   4.051  23.282  1.00  0.00           C  
ATOM     81  N   LYS A   7       0.836   5.564  23.554  1.00  0.00           N  
ATOM     82  CA  LYS A   7      -0.561   5.790  23.072  1.00  0.00           C  
ATOM     83  C   LYS A   7      -0.738   5.153  21.681  1.00  0.00           C  
ATOM     84  O   LYS A   7      -0.655   5.820  20.668  1.00  0.00           O  
ATOM     85  CB  LYS A   7      -0.808   7.317  23.029  1.00  0.00           C  
ATOM     86  CG  LYS A   7      -2.280   7.623  22.620  1.00  0.00           C  
ATOM     87  CD  LYS A   7      -2.315   8.697  21.503  1.00  0.00           C  
ATOM     88  CE  LYS A   7      -2.026  10.093  22.094  1.00  0.00           C  
ATOM     89  NZ  LYS A   7      -3.130  10.504  23.007  1.00  0.00           N  
ATOM    103  N   CYS A   8      -0.979   3.865  21.679  1.00  0.00           N  
ATOM    104  CA  CYS A   8      -1.169   3.131  20.390  1.00  0.00           C  
ATOM    105  C   CYS A   8      -2.603   3.345  19.872  1.00  0.00           C  
ATOM    106  O   CYS A   8      -3.241   4.313  20.240  1.00  0.00           O  
ATOM    107  CB  CYS A   8      -0.895   1.646  20.641  1.00  0.00           C  
ATOM    108  SG  CYS A   8      -2.192   0.572  21.296  1.00  0.00           S  
ATOM    113  N   SER A   9      -3.073   2.454  19.031  1.00  0.00           N  
ATOM    114  CA  SER A   9      -4.459   2.567  18.471  1.00  0.00           C  
ATOM    115  C   SER A   9      -4.893   1.190  17.954  1.00  0.00           C  
ATOM    116  O   SER A   9      -4.066   0.383  17.576  1.00  0.00           O  
ATOM    117  CB  SER A   9      -4.520   3.583  17.283  1.00  0.00           C  
ATOM    118  OG  SER A   9      -3.339   4.374  17.340  1.00  0.00           O  
ATOM    124  N   ARG A  10      -6.182   0.966  17.952  1.00  0.00           N  
ATOM    125  CA  ARG A  10      -6.741  -0.337  17.470  1.00  0.00           C  
ATOM    126  C   ARG A  10      -6.304  -0.622  16.028  1.00  0.00           C  
ATOM    127  O   ARG A  10      -6.024  -1.749  15.671  1.00  0.00           O  
ATOM    128  CB  ARG A  10      -8.282  -0.287  17.526  1.00  0.00           C  
ATOM    129  CG  ARG A  10      -8.754  -0.445  18.979  1.00  0.00           C  
ATOM    130  CD  ARG A  10     -10.288  -0.399  19.049  1.00  0.00           C  
ATOM    131  NE  ARG A  10     -10.820  -1.726  18.604  1.00  0.00           N  
ATOM    132  CZ  ARG A  10     -12.038  -2.112  18.894  1.00  0.00           C  
ATOM    133  NH1 ARG A  10     -12.834  -1.340  19.589  1.00  0.00           N  
ATOM    134  NH2 ARG A  10     -12.427  -3.282  18.471  1.00  0.00           N  
ATOM    148  N   LEU A  11      -6.265   0.433  15.255  1.00  0.00           N  
ATOM    149  CA  LEU A  11      -5.864   0.344  13.815  1.00  0.00           C  
ATOM    150  C   LEU A  11      -4.384   0.683  13.611  1.00  0.00           C  
ATOM    151  O   LEU A  11      -3.969   0.965  12.503  1.00  0.00           O  
ATOM    152  CB  LEU A  11      -6.774   1.313  13.012  1.00  0.00           C  
ATOM    153  CG  LEU A  11      -6.994   0.784  11.566  1.00  0.00           C  
ATOM    154  CD1 LEU A  11      -8.085  -0.313  11.564  1.00  0.00           C  
ATOM    155  CD2 LEU A  11      -7.457   1.949  10.667  1.00  0.00           C  
ATOM    167  N   MET A  12      -3.633   0.642  14.681  1.00  0.00           N  
ATOM    168  CA  MET A  12      -2.181   0.955  14.605  1.00  0.00           C  
ATOM    169  C   MET A  12      -1.424   0.208  15.699  1.00  0.00           C  
ATOM    170  O   MET A  12      -1.345   0.638  16.835  1.00  0.00           O  
ATOM    171  CB  MET A  12      -1.942   2.476  14.783  1.00  0.00           C  
ATOM    172  CG  MET A  12      -2.273   3.261  13.501  1.00  0.00           C  
ATOM    173  SD  MET A  12      -1.281   4.739  13.174  1.00  0.00           S  
ATOM    174  CE  MET A  12      -2.101   5.867  14.328  1.00  0.00           C  
ATOM    184  N   TYR A  13      -0.901  -0.916  15.290  1.00  0.00           N  
ATOM    185  CA  TYR A  13      -0.102  -1.793  16.206  1.00  0.00           C  
ATOM    186  C   TYR A  13       1.351  -1.289  16.072  1.00  0.00           C  
ATOM    187  O   TYR A  13       2.289  -2.025  15.834  1.00  0.00           O  
ATOM    188  CB  TYR A  13      -0.254  -3.253  15.739  1.00  0.00           C  
ATOM    189  CG  TYR A  13      -1.718  -3.672  15.951  1.00  0.00           C  
ATOM    190  CD1 TYR A  13      -2.662  -3.436  14.970  1.00  0.00           C  
ATOM    191  CD2 TYR A  13      -2.113  -4.284  17.126  1.00  0.00           C  
ATOM    192  CE1 TYR A  13      -3.977  -3.803  15.159  1.00  0.00           C  
ATOM    193  CE2 TYR A  13      -3.429  -4.651  17.316  1.00  0.00           C  
ATOM    194  CZ  TYR A  13      -4.370  -4.414  16.334  1.00  0.00           C  
ATOM    195  OH  TYR A  13      -5.685  -4.781  16.531  1.00  0.00           O  
ATOM    205  N   ASP A  14       1.439   0.005  16.244  1.00  0.00           N  
ATOM    206  CA  ASP A  14       2.699   0.803  16.172  1.00  0.00           C  
ATOM    207  C   ASP A  14       3.786   0.311  17.119  1.00  0.00           C  
ATOM    208  O   ASP A  14       4.954   0.579  16.914  1.00  0.00           O  
ATOM    209  CB  ASP A  14       2.350   2.270  16.496  1.00  0.00           C  
ATOM    210  CG  ASP A  14       1.292   2.338  17.620  1.00  0.00           C  
ATOM    211  OD1 ASP A  14       1.595   1.827  18.687  1.00  0.00           O  
ATOM    212  OD2 ASP A  14       0.241   2.897  17.349  1.00  0.00           O  
ATOM    217  N   CYS A  15       3.360  -0.395  18.132  1.00  0.00           N  
ATOM    218  CA  CYS A  15       4.330  -0.929  19.125  1.00  0.00           C  
ATOM    219  C   CYS A  15       5.342  -1.858  18.440  1.00  0.00           C  
ATOM    220  O   CYS A  15       4.992  -2.565  17.513  1.00  0.00           O  
ATOM    221  CB  CYS A  15       3.547  -1.671  20.193  1.00  0.00           C  
ATOM    222  SG  CYS A  15       1.900  -1.038  20.589  1.00  0.00           S  
ATOM    227  N   CYS A  16       6.562  -1.831  18.920  1.00  0.00           N  
ATOM    228  CA  CYS A  16       7.639  -2.692  18.328  1.00  0.00           C  
ATOM    229  C   CYS A  16       7.256  -4.165  18.234  1.00  0.00           C  
ATOM    230  O   CYS A  16       7.565  -4.825  17.260  1.00  0.00           O  
ATOM    231  CB  CYS A  16       8.906  -2.555  19.180  1.00  0.00           C  
ATOM    232  SG  CYS A  16      10.087  -1.269  18.706  1.00  0.00           S  
ATOM    237  N   THR A  17       6.590  -4.618  19.262  1.00  0.00           N  
ATOM    238  CA  THR A  17       6.151  -6.051  19.310  1.00  0.00           C  
ATOM    239  C   THR A  17       4.621  -6.232  19.305  1.00  0.00           C  
ATOM    240  O   THR A  17       4.089  -7.080  19.997  1.00  0.00           O  
ATOM    241  CB  THR A  17       6.827  -6.666  20.584  1.00  0.00           C  
ATOM    242  OG1 THR A  17       6.460  -8.039  20.604  1.00  0.00           O  
ATOM    243  CG2 THR A  17       6.276  -6.074  21.900  1.00  0.00           C  
ATOM    251  N   GLY A  18       3.964  -5.423  18.507  1.00  0.00           N  
ATOM    252  CA  GLY A  18       2.465  -5.472  18.386  1.00  0.00           C  
ATOM    253  C   GLY A  18       1.799  -5.622  19.762  1.00  0.00           C  
ATOM    254  O   GLY A  18       0.868  -6.380  19.955  1.00  0.00           O  
ATOM    258  N   SER A  19       2.341  -4.855  20.667  1.00  0.00           N  
ATOM    259  CA  SER A  19       1.896  -4.804  22.091  1.00  0.00           C  
ATOM    260  C   SER A  19       0.630  -3.966  22.289  1.00  0.00           C  
ATOM    261  O   SER A  19       0.019  -4.018  23.339  1.00  0.00           O  
ATOM    262  CB  SER A  19       3.062  -4.218  22.886  1.00  0.00           C  
ATOM    263  OG  SER A  19       3.448  -5.226  23.809  1.00  0.00           O  
ATOM    269  N   CYS A  20       0.289  -3.223  21.269  1.00  0.00           N  
ATOM    270  CA  CYS A  20      -0.920  -2.344  21.301  1.00  0.00           C  
ATOM    271  C   CYS A  20      -2.164  -3.024  21.877  1.00  0.00           C  
ATOM    272  O   CYS A  20      -2.548  -4.099  21.456  1.00  0.00           O  
ATOM    273  CB  CYS A  20      -1.233  -1.857  19.880  1.00  0.00           C  
ATOM    274  SG  CYS A  20      -2.582  -0.661  19.745  1.00  0.00           S  
ATOM    279  N   ARG A  21      -2.742  -2.346  22.835  1.00  0.00           N  
ATOM    280  CA  ARG A  21      -3.967  -2.841  23.523  1.00  0.00           C  
ATOM    281  C   ARG A  21      -5.075  -1.816  23.279  1.00  0.00           C  
ATOM    282  O   ARG A  21      -5.488  -1.083  24.160  1.00  0.00           O  
ATOM    283  CB  ARG A  21      -3.694  -2.993  25.044  1.00  0.00           C  
ATOM    284  CG  ARG A  21      -2.677  -4.145  25.298  1.00  0.00           C  
ATOM    285  CD  ARG A  21      -1.700  -3.757  26.426  1.00  0.00           C  
ATOM    286  NE  ARG A  21      -2.505  -3.278  27.596  1.00  0.00           N  
ATOM    287  CZ  ARG A  21      -2.219  -2.185  28.261  1.00  0.00           C  
ATOM    288  NH1 ARG A  21      -1.193  -1.440  27.943  1.00  0.00           N  
ATOM    289  NH2 ARG A  21      -2.998  -1.867  29.257  1.00  0.00           N  
ATOM    303  N   SER A  22      -5.493  -1.811  22.038  1.00  0.00           N  
ATOM    304  CA  SER A  22      -6.580  -0.906  21.548  1.00  0.00           C  
ATOM    305  C   SER A  22      -6.366   0.558  21.945  1.00  0.00           C  
ATOM    306  O   SER A  22      -7.308   1.311  22.107  1.00  0.00           O  
ATOM    307  CB  SER A  22      -7.931  -1.421  22.111  1.00  0.00           C  
ATOM    308  OG  SER A  22      -8.044  -2.738  21.591  1.00  0.00           O  
ATOM    314  N   GLY A  23      -5.115   0.904  22.081  1.00  0.00           N  
ATOM    315  CA  GLY A  23      -4.752   2.302  22.464  1.00  0.00           C  
ATOM    316  C   GLY A  23      -3.581   2.386  23.448  1.00  0.00           C  
ATOM    317  O   GLY A  23      -3.003   3.444  23.595  1.00  0.00           O  
ATOM    321  N   LYS A  24      -3.262   1.286  24.090  1.00  0.00           N  
ATOM    322  CA  LYS A  24      -2.126   1.284  25.073  1.00  0.00           C  
ATOM    323  C   LYS A  24      -1.114   0.188  24.717  1.00  0.00           C  
ATOM    324  O   LYS A  24      -1.452  -0.975  24.777  1.00  0.00           O  
ATOM    325  CB  LYS A  24      -2.725   1.049  26.483  1.00  0.00           C  
ATOM    326  CG  LYS A  24      -1.743   1.507  27.587  1.00  0.00           C  
ATOM    327  CD  LYS A  24      -1.705   3.058  27.658  1.00  0.00           C  
ATOM    328  CE  LYS A  24      -0.331   3.582  27.212  1.00  0.00           C  
ATOM    329  NZ  LYS A  24      -0.510   4.785  26.353  1.00  0.00           N  
ATOM    343  N   CYS A  25       0.100   0.549  24.362  1.00  0.00           N  
ATOM    344  CA  CYS A  25       1.085  -0.513  24.011  1.00  0.00           C  
ATOM    345  C   CYS A  25       1.463  -1.270  25.290  1.00  0.00           C  
ATOM    346  O   CYS A  25       1.692  -0.702  26.337  1.00  0.00           O  
ATOM    347  CB  CYS A  25       2.313   0.160  23.356  1.00  0.00           C  
ATOM    348  SG  CYS A  25       2.192   0.671  21.625  1.00  0.00           S  
HETATM  353  N   NH2 A  26       1.522  -2.569  25.237  1.00  0.00           N  
HETATM 2711  O   DUM  2711     -12.000  -4.000  15.400                          
HETATM 2712  O   DUM  2712     -12.000  -2.000  15.400                          
HETATM 2713  O   DUM  2713     -12.000   0.000  15.400                          
HETATM 2714  O   DUM  2714     -12.000   2.000  15.400                          
HETATM 2715  O   DUM  2715     -12.000   4.000  15.400                          
HETATM 2790  O   DUM  2790     -10.000  -8.000  15.400                          
HETATM 2791  O   DUM  2791     -10.000  -6.000  15.400                          
HETATM 2792  O   DUM  2792     -10.000  -4.000  15.400                          
HETATM 2793  O   DUM  2793     -10.000  -2.000  15.400                          
HETATM 2794  O   DUM  2794     -10.000   0.000  15.400                          
HETATM 2795  O   DUM  2795     -10.000   2.000  15.400                          
HETATM 2796  O   DUM  2796     -10.000   4.000  15.400                          
HETATM 2797  O   DUM  2797     -10.000   6.000  15.400                          
HETATM 2798  O   DUM  2798     -10.000   8.000  15.400                          
HETATM 2870  O   DUM  2870      -8.000 -10.000  15.400                          
HETATM 2871  O   DUM  2871      -8.000  -8.000  15.400                          
HETATM 2872  O   DUM  2872      -8.000  -6.000  15.400                          
HETATM 2873  O   DUM  2873      -8.000  -4.000  15.400                          
HETATM 2874  O   DUM  2874      -8.000  -2.000  15.400                          
HETATM 2875  O   DUM  2875      -8.000   0.000  15.400                          
HETATM 2876  O   DUM  2876      -8.000   2.000  15.400                          
HETATM 2877  O   DUM  2877      -8.000   4.000  15.400                          
HETATM 2878  O   DUM  2878      -8.000   6.000  15.400                          
HETATM 2879  O   DUM  2879      -8.000   8.000  15.400                          
HETATM 2880  O   DUM  2880      -8.000  10.000  15.400                          
HETATM 2951  O   DUM  2951      -6.000 -10.000  15.400                          
HETATM 2952  O   DUM  2952      -6.000  -8.000  15.400                          
HETATM 2953  O   DUM  2953      -6.000  -6.000  15.400                          
HETATM 2954  O   DUM  2954      -6.000  -4.000  15.400                          
HETATM 2955  O   DUM  2955      -6.000  -2.000  15.400                          
HETATM 2956  O   DUM  2956      -6.000   0.000  15.400                          
HETATM 2957  O   DUM  2957      -6.000   2.000  15.400                          
HETATM 2958  O   DUM  2958      -6.000   4.000  15.400                          
HETATM 2959  O   DUM  2959      -6.000   6.000  15.400                          
HETATM 2960  O   DUM  2960      -6.000   8.000  15.400                          
HETATM 2961  O   DUM  2961      -6.000  10.000  15.400                          
HETATM 3031  O   DUM  3031      -4.000 -12.000  15.400                          
HETATM 3032  O   DUM  3032      -4.000 -10.000  15.400                          
HETATM 3033  O   DUM  3033      -4.000  -8.000  15.400                          
HETATM 3034  O   DUM  3034      -4.000  -6.000  15.400                          
HETATM 3035  O   DUM  3035      -4.000  -4.000  15.400                          
HETATM 3036  O   DUM  3036      -4.000  -2.000  15.400                          
HETATM 3037  O   DUM  3037      -4.000   0.000  15.400                          
HETATM 3038  O   DUM  3038      -4.000   2.000  15.400                          
HETATM 3039  O   DUM  3039      -4.000   4.000  15.400                          
HETATM 3040  O   DUM  3040      -4.000   6.000  15.400                          
HETATM 3041  O   DUM  3041      -4.000   8.000  15.400                          
HETATM 3042  O   DUM  3042      -4.000  10.000  15.400                          
HETATM 3043  O   DUM  3043      -4.000  12.000  15.400                          
HETATM 3112  O   DUM  3112      -2.000 -12.000  15.400                          
HETATM 3113  O   DUM  3113      -2.000 -10.000  15.400                          
HETATM 3114  O   DUM  3114      -2.000  -8.000  15.400                          
HETATM 3115  O   DUM  3115      -2.000  -6.000  15.400                          
HETATM 3116  O   DUM  3116      -2.000  -4.000  15.400                          
HETATM 3117  O   DUM  3117      -2.000  -2.000  15.400                          
HETATM 3118  O   DUM  3118      -2.000   0.000  15.400                          
HETATM 3119  O   DUM  3119      -2.000   2.000  15.400                          
HETATM 3120  O   DUM  3120      -2.000   4.000  15.400                          
HETATM 3121  O   DUM  3121      -2.000   6.000  15.400                          
HETATM 3122  O   DUM  3122      -2.000   8.000  15.400                          
HETATM 3123  O   DUM  3123      -2.000  10.000  15.400                          
HETATM 3124  O   DUM  3124      -2.000  12.000  15.400                          
HETATM 3193  O   DUM  3193       0.000 -12.000  15.400                          
HETATM 3194  O   DUM  3194       0.000 -10.000  15.400                          
HETATM 3195  O   DUM  3195       0.000  -8.000  15.400                          
HETATM 3196  O   DUM  3196       0.000  -6.000  15.400                          
HETATM 3197  O   DUM  3197       0.000  -4.000  15.400                          
HETATM 3198  O   DUM  3198       0.000  -2.000  15.400                          
HETATM 3199  O   DUM  3199       0.000   0.000  15.400                          
HETATM 3200  O   DUM  3200       0.000   2.000  15.400                          
HETATM 3201  O   DUM  3201       0.000   4.000  15.400                          
HETATM 3202  O   DUM  3202       0.000   6.000  15.400                          
HETATM 3203  O   DUM  3203       0.000   8.000  15.400                          
HETATM 3204  O   DUM  3204       0.000  10.000  15.400                          
HETATM 3205  O   DUM  3205       0.000  12.000  15.400                          
HETATM 3274  O   DUM  3274       2.000 -12.000  15.400                          
HETATM 3275  O   DUM  3275       2.000 -10.000  15.400                          
HETATM 3276  O   DUM  3276       2.000  -8.000  15.400                          
HETATM 3277  O   DUM  3277       2.000  -6.000  15.400                          
HETATM 3278  O   DUM  3278       2.000  -4.000  15.400                          
HETATM 3279  O   DUM  3279       2.000  -2.000  15.400                          
HETATM 3280  O   DUM  3280       2.000   0.000  15.400                          
HETATM 3281  O   DUM  3281       2.000   2.000  15.400                          
HETATM 3282  O   DUM  3282       2.000   4.000  15.400                          
HETATM 3283  O   DUM  3283       2.000   6.000  15.400                          
HETATM 3284  O   DUM  3284       2.000   8.000  15.400                          
HETATM 3285  O   DUM  3285       2.000  10.000  15.400                          
HETATM 3286  O   DUM  3286       2.000  12.000  15.400                          
HETATM 3355  O   DUM  3355       4.000 -12.000  15.400                          
HETATM 3356  O   DUM  3356       4.000 -10.000  15.400                          
HETATM 3357  O   DUM  3357       4.000  -8.000  15.400                          
HETATM 3358  O   DUM  3358       4.000  -6.000  15.400                          
HETATM 3359  O   DUM  3359       4.000  -4.000  15.400                          
HETATM 3360  O   DUM  3360       4.000  -2.000  15.400                          
HETATM 3361  O   DUM  3361       4.000   0.000  15.400                          
HETATM 3362  O   DUM  3362       4.000   2.000  15.400                          
HETATM 3363  O   DUM  3363       4.000   4.000  15.400                          
HETATM 3364  O   DUM  3364       4.000   6.000  15.400                          
HETATM 3365  O   DUM  3365       4.000   8.000  15.400                          
HETATM 3366  O   DUM  3366       4.000  10.000  15.400                          
HETATM 3367  O   DUM  3367       4.000  12.000  15.400                          
HETATM 3437  O   DUM  3437       6.000 -10.000  15.400                          
HETATM 3438  O   DUM  3438       6.000  -8.000  15.400                          
HETATM 3439  O   DUM  3439       6.000  -6.000  15.400                          
HETATM 3440  O   DUM  3440       6.000  -4.000  15.400                          
HETATM 3441  O   DUM  3441       6.000  -2.000  15.400                          
HETATM 3442  O   DUM  3442       6.000   0.000  15.400                          
HETATM 3443  O   DUM  3443       6.000   2.000  15.400                          
HETATM 3444  O   DUM  3444       6.000   4.000  15.400                          
HETATM 3445  O   DUM  3445       6.000   6.000  15.400                          
HETATM 3446  O   DUM  3446       6.000   8.000  15.400                          
HETATM 3447  O   DUM  3447       6.000  10.000  15.400                          
HETATM 3518  O   DUM  3518       8.000 -10.000  15.400                          
HETATM 3519  O   DUM  3519       8.000  -8.000  15.400                          
HETATM 3520  O   DUM  3520       8.000  -6.000  15.400                          
HETATM 3521  O   DUM  3521       8.000  -4.000  15.400                          
HETATM 3522  O   DUM  3522       8.000  -2.000  15.400                          
HETATM 3523  O   DUM  3523       8.000   0.000  15.400                          
HETATM 3524  O   DUM  3524       8.000   2.000  15.400                          
HETATM 3525  O   DUM  3525       8.000   4.000  15.400                          
HETATM 3526  O   DUM  3526       8.000   6.000  15.400                          
HETATM 3527  O   DUM  3527       8.000   8.000  15.400                          
HETATM 3528  O   DUM  3528       8.000  10.000  15.400                          
HETATM 3600  O   DUM  3600      10.000  -8.000  15.400                          
HETATM 3601  O   DUM  3601      10.000  -6.000  15.400                          
HETATM 3602  O   DUM  3602      10.000  -4.000  15.400                          
HETATM 3603  O   DUM  3603      10.000  -2.000  15.400                          
HETATM 3604  O   DUM  3604      10.000   0.000  15.400                          
HETATM 3605  O   DUM  3605      10.000   2.000  15.400                          
HETATM 3606  O   DUM  3606      10.000   4.000  15.400                          
HETATM 3607  O   DUM  3607      10.000   6.000  15.400                          
HETATM 3608  O   DUM  3608      10.000   8.000  15.400                          
HETATM 3683  O   DUM  3683      12.000  -4.000  15.400                          
HETATM 3684  O   DUM  3684      12.000  -2.000  15.400                          
HETATM 3685  O   DUM  3685      12.000   0.000  15.400                          
HETATM 3686  O   DUM  3686      12.000   2.000  15.400                          
HETATM 3687  O   DUM  3687      12.000   4.000  15.400                          
