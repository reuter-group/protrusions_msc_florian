## Readme

- The **result_and_feature_datasets** folder contains all my computed feature datasets and results.
- The **other_scripts** folder contains some scripts I expirimented with, mostly to learn neccessary python and chimera.
- The **opm-data** folder contains all raw proteins files used in this project, downloaded from the opm database. I provide the original pdb files in order to make a reproduction of my results easier as pdb files in the opm database may change.
- The **jupyter_workspaces** folder contains all my code and workspaces used to make my analysis and generate my figures.
- The **fuglebakkReuter_datasets** folder contains the feature datasets from Fuglebakk and Reuter (2018), see <https://www.ncbi.nlm.nih.gov/pmc/articles/PMC6080788/>
- The **convexhull_plugin** folder contains the convex hull plugin I have written for UCSF Chimera. 
- The **tm_ids.csv** file contains all pdb ID's of the reference dataset and corresponds to the pdb files in /opm_data/tm
- The **periph_ids.csv** file contains all pdb ID's of the peripheral protein dataset and corresponds to the pdb files in /opm_data/periph

<br>

### About my feature datasets in the results_and_feature_datasets folder

- The **periph_protrusion.csv** and **tm_protrusion.csv** contains all computed protrusions, for the peripheral proteins and the reference data (tm) respectively. It also gives an overview about what beta-carbons not became protrusions.
- The **periph_neighbourhood_data.csv** and **tm_neighbourhood_data.csv** contains all computed neighbourhoods for every amino acid and every protein in the data, for peripheral proteins and the reference data (tm) respectively.
- The **periph_sasa_data.csv** and **tm_sasa_data.csv** contain all computed SASA and RSA values, for every amino acids in every protein from the data. The RSA is marked as sasa_percentage column.

