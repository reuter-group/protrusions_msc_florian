import os
import numpy as np
import pandas as pd
from biopandas.pdb import PandasPdb
from scipy.spatial import ConvexHull
import scipy.spatial.distance as scidist

import time

DISTANCE = 10
DENSITY = 22
HYDROPHOBICS = ['LEU', 'ILE', 'PHE', 'TYR', 'TRP', 'CYS', 'MET']


def findHydrophobicProtrusions(pdb, atomTable):
    # Get idx size
    N = len(atomTable)

    # Matrix for all atoms
    m = atomTable[["x_coord", "y_coord", "z_coord"]].to_numpy()

    # Compute convex hull
    hull = ConvexHull(m)

    # Compute distances
    # dist = np.repeat(np.repeat(np.array([[0.0]]), N, 1), N, 0)
    # for idx, coord in enumerate(m):
    #     mm = np.repeat(np.array([[coord[0], coord[1], coord[2]]]), N, 0)
    #     dist[idx] = np.sqrt(np.sum((mm - m) ** 2, axis=1))

    distvector = scidist.pdist(m)
    dist = scidist.squareform(distvector)

    # Find low density atoms (protrusions) on the convex hull
    protrusions = []
    for idx, row in enumerate(dist):
        if idx in hull.vertices:
            if row[row <= DISTANCE].shape[0] - 1 < DENSITY:
                protrusions.append(idx)

    # Find hydrophobic atoms with low density (hydrophobic protrusions)
    tmp = atomTable.loc[protrusions]
    hydrophobic_protrusions = tmp[tmp["residue_name"].isin(HYDROPHOBICS)].index.tolist()

    # Build & return data of interest
    hp = atomTable.loc[hydrophobic_protrusions]
    return pd.DataFrame({"PDB": pdb,
                         "NUMBER_OF_PROTRUSIONS": len(protrusions),
                         "NUMBER_OF_HYDROPHOBIC_PROTRUSIONS": len(hydrophobic_protrusions),
                         "ATOM_INDICES_OF_HYDROPHOBIC_PROTRUSIONS": ", ".join(
                             [str(x) for x in hp["index"].tolist()]),
                         "RESIDUE_NAMES_OF_HYDROPHOBIC_PROTRUSIONS": ", ".join(
                             [str(x) for x in hp["residue_name"].tolist()])
                         }, index=[0])


def extract_atom_table(path):
    """
    This function extracts the atom table of a given pdb, returns just rows of CA and CB.
    :param path: Path to the pdb file
    :return: pandas.core.frame.DataFrame, containing every CA and CB atom for this pdb
    """
    pdb = PandasPdb().read_pdb(path)
    atomTable = pdb.df["ATOM"][["atom_number", "atom_name", "residue_name", "x_coord", "y_coord", "z_coord"]]
    atomTable = atomTable[0:atomTable.iloc[-1]["atom_number"]]
    atomTable = atomTable.query("atom_name in ['CA', 'CB']")
    return atomTable.reset_index()


def readIDs(filepath):
    """
    Read / parse a text file containing pdb ID's

    :param filepath: The path to the file to parse / read
    :return: The list of pdb ID's parsed
    """
    id_list = []
    with open(filepath) as f:
        for line in f:
            id_list.append(line.replace("\n", ""))

    return id_list


# =====================================================================================================================
# Main
# =====================================================================================================================

tm = readIDs("tm_ids.csv")
periph = readIDs("periph_ids.csv")

data_tm = pd.DataFrame()
data_periph = pd.DataFrame()

s = time.time()
for idx, pdb in enumerate(tm):
    atomTable = extract_atom_table("tm/" + pdb + ".pdb")
    data_tm = data_tm.append(findHydrophobicProtrusions(pdb, atomTable), ignore_index=True)
    print "[INFO] Progress at: " + str(round(float(idx+1)/float(len(tm))*100, 2)) + " %"

data_tm.to_csv("basicFeatures_tm.csv", sep="\t", encoding="utf-8")
e = time.time()
print "============================="
print "Computation time: " + "{:.2}".format(e-s) + "s"

s = time.time()
for idx, pdb in enumerate(periph):
    atomTable = extract_atom_table("periph/" + pdb + ".pdb")
    data_periph = data_periph.append(findHydrophobicProtrusions(pdb, atomTable), ignore_index=True)
    print "[INFO] Progress at: " + str(round(float(idx+1)/float(len(periph))*100, 2)) + " %"

data_periph.to_csv("basicFeatures_periph.csv", sep="\t", encoding="utf-8")
e = time.time()
print "============================="
print "Computation time: " + "{:.2}".format(e-s) + "s"
