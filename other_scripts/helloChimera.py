import chimera

# Open model
opened = chimera.openModels.open('~/pathToFile/4fun.pdb')

# List over opened models
chimera.openModels.count()
chimera.openModels.list()

# Import Bild object from file
chimera.openModels.open('~/pathToFile/4fun.bld', identifyAs="nameOfObject")

# Create new primitive bild
from StringIO import StringIO
mesh = StringIO(".box 5 5 5 10 10 10 \n")
chimera.openModels.open(mesh, type="Bild", identifyAs="nameOfObject")

# Change color - here i is the index in the openModel list
from chimera.colorTable import getColorByName
i = 0
chimera.openModels.list()[i].color = getColorByName("yellow")

# Translate, rotate, scale object using chimera cmd's
chimera.runCommand("move x 5 models #0")
chimera.runCommand("turn z 45 models #2")
 
# to execute code from a py file in chimera - IDLE
# execfile('PythonProjects/TestProject/helloChimera.py')