import os
from Bio.PDB import *


def fetchData(list_of_ids, path):
    """
    Download / fetch all pdb's on the list and save to the given path

    :param list_of_ids: List of pdb ID's
    :param path: The filepath where to save the retrieved pdb files to
    :return: List of IDs that are NOT fetched
    """
    pdbl = PDBList()
    not_retrieved = []

    for ID in list_of_ids:
        # Fetch
        pdbl.retrieve_pdb_file(ID, pdir=path, file_format="pdb")

        # Try to fetch with obsolete true
        tmp = path + "pdb" + ID + ".ent"
        if not os.path.isfile(tmp):
            pdbl.retrieve_pdb_file(ID, pdir=path, file_format="pdb", obsolete=True)

        # If not fetched, add to not_retrieved list
        if not os.path.isfile(tmp):
            not_retrieved.append(ID)

        # Rename
        else:
            os.rename(tmp, (path + ID + ".pdb"))

    return not_retrieved


def readIDs(filepath):
    """
    Read / parse a text file containing pdb ID's

    :param filepath: The path to the file to parse / read
    :return: The list of pdb ID's parsed
    """
    id_list = []
    with open(filepath) as f:
        for line in f:
            id_list.append(line.replace("\n", ""))

    return id_list
