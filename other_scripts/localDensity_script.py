import chimera
import numpy as np

# atoms
prot = chimera.openModels.list()[0]
atoms = prot.atoms

# subset c-alpha and c-beta atoms
rel_atoms = [atom for atom in atoms if atom.name == "CA" or atom.name == "CB"]
N = len(rel_atoms)

# Matrix for all atom coords
m = np.array([[a.coord().x, a.coord().y, a.coord().z] for a in rel_atoms])

# Very fast distance computation
dist = np.repeat(np.repeat(np.array([[0.0]]), N, 1), N, 0)
for idx, atom in enumerate(rel_atoms):
    mm = np.repeat(np.array([[atom.coord().x, atom.coord().y, atom.coord().z]]), N, 0)
    dist[idx] = np.sqrt(np.sum((mm - m) ** 2, axis=1))

# subset atoms that are convexhull vertices
# TODO -- NB: This is not safe & needs rewrite
conv_atoms = [atom for atom in atoms if atom.display]

# low density atoms on the convex hull, 10AA = 1nm and with n < 22
lowDens_atoms = []
for idx, row in enumerate(dist):
    atom = rel_atoms[idx]
    if row[row < 10].shape[0] < 22 and atom in conv_atoms:
        print("low density at: " + str(rel_atoms[idx]))
        atom.color = chimera.colorTable.getColorByName("green")
        lowDens_atoms.append(atom)

# Mark hydrophobes with low density on the convex hull
HYDROPHOBICS = ["LEU", "ILE", "PHE", "TYR", "TRP", "CYS", "MET"]
for atom in lowDens_atoms:
    if atom.residue.type in HYDROPHOBICS:
        atom.color = chimera.colorTable.getColorByName("orange")