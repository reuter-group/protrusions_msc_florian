import sys
sys.path.append("protrusions_msc_florian/ChimeraScript_ConvexHull/")

from convexhull3d import ConvexHull3D
from StringIO import StringIO
import numpy
import chimera

# Get already opened protein model
protein = chimera.openModels.list()[0]

# Get the coordinates for atoms of interest (C-alpha & C-beta)
coords = [[atom.coord().x, atom.coord().y, atom.coord().z] for atom in protein.atoms if (atom.name == "CA") or (atom.name == "CB")]

# Compute the convex hull 
convHull = ConvexHull3D(coords)

# Translate to Chimera's *.bild foromat and draw the convex hull in Chimera
#convHullBild = ".color 0.6 0.745 0.88\n.transparency 0.06\n"
convHullBild = ""
for triangle in convHull.get_convexhull():
    polygon = [coords[tri] for tri in triangle]
    line = ".polygon " + str(polygon[0]) + " " + str(polygon[1]) + " " + str(polygon[2]) + "\n"
    line = line.replace("[", "").replace("]", "").replace(",","")
    convHullBild += line
    
convHullBild_IO = StringIO(convHullBild)
chimera.openModels.open(convHullBild_IO, type="Bild", identifyAs="convexHull_CA_and_CB")

# Display just C-alpha and C-beta which are vertices of the convex hull
atomIdx = numpy.array(convHull.get_convexhull())
unique_atomIdx = numpy.unique(atomIdx)

general_atoms = [atom for atom in protein.atoms if (atom.name == "CA") or (atom.name == "CB")]
convHull_atoms = [general_atoms[idx] for idx in unique_atomIdx]

for atom in convHull_atoms:
    if(atom.name == "CA"):
		atom.color = chimera.colorTable.getColorByName("orange red")
		atom.drawMode = chimera.Atom.Ball
		atom.display = True
    if(atom.name == "CB"):
		atom.color = chimera.colorTable.getColorByName("orange")
		atom.drawMode = chimera.Atom.Ball
		atom.display = True

chimera.runCommand("setattr m ballScale 0.4")
