from StringIO import StringIO

import chimera
import _surface as graphics
import numpy as np
from convexhull3d import ConvexHull3D

CONVHULL = None
COORDS = None
VERTICES = None
PROTEIN = None
LOWDENS_ATOMS = None
DIST = None
CACHED_CONV_VERTICES = None

HYDROPHOBICS = ["LEU", "ILE", "PHE", "TYR", "TRP", "CYS", "MET"]
AROMATICS = ["TYR", "TRP", "PHE", "HIS"]
CHARGED_POS = ["LYS", "HIS", "ARG"]
CHARDED_NEG = ["ASP", "GLU"]


def createConvexHull(protein, atom_selection, amino_selection, among_selection, convHullColor, vertexColor,
                     showVertices, maxDistance, maxDensity, showProtrusions, showHydrophobicProtrusions,
                     protrusionColor, hydrophobicProtrusionColor, temporary=True):
    """
    Create a convex hull

    :param protein: chimera.Molecule, the protein you want to create the convex hull on.
    :param atom_selection: String, the main atom selection used to find atoms of interest, either "CA", "CB" or "both".
    :param amino_selection: String, the amino acid selection to filter atoms, either "hydrophobic", "aromatic",
    "charged_positive" or "charged_negative"
    :param among_selection: Bool, true if you only want to compute a convex hull among current viewport selection.
    :param convHullColor: chimera.MaterialColor, the color of the convex hull
    :param vertexColor: chimera.MaterialColor, the color of all vertices
    :param showVertices: Bool, true if you want to display all convex hull vertices
    :param maxDistance: Int, the max distance used to compute local density
    :param maxDensity: Int, the max density used to compute local low density
    :param showProtrusions: Bool, true if you want to show the protrusions
    :param showHydrophobicProtrusions: Bool, true if you want to show the hydrophobic protrusions
    :param protrusionColor: chimera.MaterialColor, the color of protrusion atoms
    :param hydrophobicProtrusionColor: chimera.MaterialColor, the color of hydrophobic protrusions
    :param temporary: Bool, true if the computed convex becomes marked as temporary
    :return: None
    """

    import time
    bb = time.time()
    bbb = 0
    print "\n"

    # Don't generate something if arguments are wrong
    _checkArguments(protein, atom_selection, amino_selection, convHullColor, vertexColor, showVertices)

    # Set Protein
    global PROTEIN
    PROTEIN = protein

    # Check if already computed correctly and non temporary
    if not temporary:
        keepConvexHull()
        return

    # Get atoms
    if among_selection:
        atoms = [atom for atom in chimera.selection.currentAtoms() if atom.molecule == protein]
        atoms = filterAtoms(atoms, atom_selection)
    else:
        atoms = getAtoms(protein, atom_selection)

    # Filter atoms based on amino acid selection
    if amino_selection is not None:
        atoms = _filterAtomsBasedOnAminoAcidType(atoms, amino_selection)

    # TODO -- use this check to generate a different name to work on, do not overwrite any earlier convex hull
    # Generate name and delete any earlier convex hull
    genName = _generateConvexHullName(protein, temporary)
    _deleteConvexHull(protein, genName)
    resetAtomStyle(protein)

    # Avoid computation on less than 4 atoms
    if len(atoms) < 4:
        return

    # Get relevant atom coordinates & compute the convex hull
    global CONVHULL, COORDS
    COORDS = [[atom.coord().x, atom.coord().y, atom.coord().z] for atom in atoms]

    b = time.time()
    CONVHULL = ConvexHull3D(COORDS)
    CONVHULL.calc_convexhull()
    e = time.time()
    print '[INFO] Computation of convex hull: \t',
    print('{:.10f}'.format(e-b))
    bbb += e-b

    # Find convex hull vertices
    b = time.time()
    from itertools import chain
    uniques = list(set(chain(*CONVHULL.get_convexhull())))

    global VERTICES
    VERTICES = [atoms[idx] for idx in uniques]
    if len(VERTICES) < 1:
        raise ValueError("No Vertices, fix that mess!")
    e = time.time()
    print '[INFO] Finding convex hull vertices: \t',
    print('{:.10f}'.format(e-b))
    bbb += e - b

    # Draw the convex hull
    b = time.time()
    _drawConvexHull(convHullColor, genName)
    e = time.time()
    print '[INFO] Draw convex hull: \t\t',
    print('{:.10f}'.format(e-b))
    bbb += e - b

    # Compute protrusion
    b = time.time()
    _computeProtrusion(maxDistance, maxDensity)
    e = time.time()
    print '[INFO] Computation of protrusions: \t',
    print('{:.10f}'.format(e-b))
    bbb += e - b

    # Draw vertices if wanted
    drawVertices(showVertices, showProtrusions, showHydrophobicProtrusions, vertexColor,
                 protrusionColor, hydrophobicProtrusionColor)

    e = time.time()
    print '[Info] Other time consumption:\t\t',
    print ('{:.10f}'.format(e-bb-bbb))
    print '===================================================='
    print '[INFO] Full convex hull creation: \t',
    print('{:.10f}'.format(e-bb))


def _checkArguments(protein, atomSel, aminSel, convHullColor, vertexColor, showVertices):
    """
    This helper function is used to check the main arguments. See createConvexHull function for documentation.
    """

    # Check protein
    if protein.__class__ != chimera.Molecule:
        raise ValueError("The protein is not a correct chimera molecule!")

    # Check atom selection
    if atomSel is not "CA" and atomSel is not "CB" and atomSel is not "both":
        raise ValueError("Atom selection is not valid: " + atomSel)

    # Check amino acid selection
    if (aminSel is not "hydrophobic" and aminSel is not "aromatic" and aminSel is not "charged_positive"
            and aminSel is not "charged_negative" and aminSel is not None):
        raise ValueError("Amino acid selection is not valid: " + aminSel)

    # Check colors
    if not isinstance(convHullColor, chimera.MaterialColor):
        raise ValueError("Illegal convex hull color object!")

    if not isinstance(vertexColor, chimera.MaterialColor):
        raise ValueError("Illegal vertex color object!")

    # Check showVertices
    if (showVertices != 0) and (showVertices != 1):
        raise ValueError("Illegal showVertices value, should be 0 or 1!")


def _deleteConvexHull(protein, genName):
    """
    Delete convex hulls of interest

    :param protein: chimera.Molecule, the protein you want to delete the convex hull for.
    :param genName: String, the generated convex hull name for the convex hull you want to delete.
    :return: None
    """

    # Find any convex hull for the given protein & configuration
    oldConvexHulls = [hull for hull in chimera.openModels.list() if (hull.__class__ == graphics.SurfaceModel)
                      and ((protein.name in hull.name) or (genName in hull.name))]

    # Delete
    for hull in oldConvexHulls:
        hull.destroy()

    # Just do something random to update the viewport
    chimera.runCommand("turn z 0 models #0")


def keepConvexHull():
    """
    Keep the convex hull, becomes not marked as temporary anymore

    :return: None
    """

    # Find convex hull for the given protein & configuration
    global PROTEIN
    genName = _generateConvexHullName(PROTEIN, True)
    hull = [hull for hull in chimera.openModels.list() if (hull.__class__ == graphics.SurfaceModel)
                      and ((PROTEIN.name in hull.name) or (genName in hull.name))][0]
    hull.name = _generateConvexHullName(PROTEIN, False)


def deleteAllTemporaryConvexHull():
    """
    Delete all temporary convex hulls

    :return: None
    """

    # Find all temporary marked convex hull objects
    hulls = [hull for hull in chimera.openModels.list() if (hull.__class__ == graphics.SurfaceModel)
             and "Temporary Convex Hull" in hull.name]

    # Delete
    for hull in hulls:
        hull.destroy()

    # Just do something random to update the viewport
    chimera.runCommand("turn z 0 models #0")


def changeColorOfConvexHull(protein, color):
    """
    Change the color of a existing convex hull object

    :param protein: chimera.Molecule, the protein you want to catch the convex hull from
    :param color: chimera.MaterialColor, the color of the convex hull
    :return: None
    """
    # Check colors
    if not isinstance(color, chimera.MaterialColor):
        raise ValueError("Illegal convex hull color object!")

    # Find & check if there is any convex hull to color
    genName = _generateConvexHullName(protein)
    hull = [hull for hull in chimera.openModels.list() if (hull.__class__ == graphics.SurfaceModel)
            and (protein.name in hull.name) and (genName in hull.name)]

    if len(hull) != 1:
        return
    else:
        hull = hull[0]

    # Set color
    for piece in hull.surfacePieces:
        piece.color = color.rgba()


def getAtoms(protein, atomSel):
    """
    Filter & return the list of atoms of interest

    :param protein: chimera.Molecule, the protein you want to get the atoms from.
    :param atomSel: String, the main atom selection used to find atoms of interest, either "CA", "CB" or "both".
    :return: List of chimera.Atom, containing all atoms of interest
    """

    if atomSel == "both":
        return [atom for atom in protein.atoms if (atom.name == "CA") or (atom.name == "CB")]
    else:
        return [atom for atom in protein.atoms if (atom.name == atomSel)]


def filterAtoms(atoms, atomSel):
    """
    Filter & return the list of atoms of interest

    :param atoms: List of chimera.Atom, containing all atoms to filter
    :param atomSel: String, the main atom selection used to find atoms of interest, either "CA", "CB" or "both".
    :return: List of chimera.Atom, containing all atoms of interest
    """

    if atomSel == "both":
        return [atom for atom in atoms if (atom.name == "CA") or (atom.name == "CB")]
    else:
        return [atom for atom in atoms if (atom.name == atomSel)]


def _filterAtomsBasedOnAminoAcidType(atoms, type):
    """
    Filter a list of atoms based on which type of amino acid they belong to

    :param atoms: List of chimera.Atom, containing all atoms to filter
    :param type:  String, the type of amino acids to filter atoms on , either "hydrophobic", "aromatic",
    "charged_positive" or "charged_negative"
    :return: List of chimera.Atom, containing all atoms of interest
    """

    # Get correct list of amino acids based on type
    if type == "hydrophobic":
        list = HYDROPHOBICS
    elif type == "aromatic":
        list = AROMATICS
    elif type == "charged_positive":
        list = CHARGED_POS
    else:
        list = CHARDED_NEG

    # Filter atoms & return
    return [atom for atom in atoms if (atom.residue.type in list)]


def resetAtomStyle(protein):
    """
    Help function to reset any atom style

    TODO -- You may not want to reset the atom style at all, delete this function?
    TODO -- You may just remember the visual state before using the plugin and reset to that state

    :param protein: chimera.Molecule, the protein you want to reset the atom style for.
    :return: None
    """

    # Reset display
    for atom in protein.atoms:
        atom.display = False

    # Reset color
    chimera.colorByElement(protein)


def resetAtomStyleOnConvexHull():
    """
    Reset any atom style on the current convex hull

    :return: None
    """

    # Reset visibility
    global VERTICES, PROTEIN
    for atom in VERTICES:
        atom.display = False

    # Reset color
    chimera.colorByElement(PROTEIN)


def _drawConvexHull(color, genName):
    """
    Draw a convex hull in chimeras viewport

    :param color: chimera.MaterialColor, the color of the convex hull.
    :param genName: String, the generated convex hull name for the convex hull.
    :return: None
    """

    global COORDS, CONVHULL
    triangles = CONVHULL.get_convexhull()
    hull = graphics.SurfaceModel()
    for tri in triangles:
        coords = [COORDS[tri[0]], COORDS[tri[1]], COORDS[tri[2]]]
        hull.addPiece(coords, [[0, 1, 2]], color.rgba())
    hull.name = genName
    chimera.openModels.add([hull])


def _generateConvexHullName(protein, temporary=True):
    """
    Generate a name to identify your convex hull object based on protein and selection.

    :param protein: chimera.Molecule, the protein that the convex hull name is for
    :return: String, the generated name for your convex hull of interest
    """

    name = ""
    if temporary:
        name = "Temporary "

    name += "Convex Hull (" + protein.name + ")"
    return name


def _computeProtrusion(distance, density):
    """
    Compute if a convex hull vertex is a protrusions - for all vertices on the convex hull

    :param distance: Int, the distance within a protrusion is satisfied
    :param density: Int, the max number of other CA & CB atoms that is restricting the protrusion
    :return: None
    """

    # Use globals
    global VERTICES, PROTEIN, LOWDENS_ATOMS

    # Subset c-alpha and c-beta atoms
    atoms = PROTEIN.atoms
    rel_atoms = [atom for atom in atoms if atom.name == "CA" or atom.name == "CB"]
    N = len(rel_atoms)

    # Matrix for all atom coords
    m = np.array([[a.coord().x, a.coord().y, a.coord().z] for a in rel_atoms])

    # Very fast distance computation for all Vertices
    global DIST
    DIST = np.repeat(np.repeat(np.array([[0.0]]), N, 1), N, 0)
    for idx, atom in enumerate(rel_atoms):
        mm = np.repeat(np.array([[atom.coord().x, atom.coord().y, atom.coord().z]]), N, 0)
        DIST[idx] = np.sqrt(np.sum((mm - m) ** 2, axis=1))

    # low density atoms on the convex hull
    LOWDENS_ATOMS = []
    for idx, row in enumerate(DIST):
        atom = rel_atoms[idx]

        # Atom has to be on the convex hull
        if atom in VERTICES:

            # Atom has to be CB
            if atom.name == "CB":

                # Residue has to have low density
                if row[row < distance].shape[0] - 1 < density:
                    LOWDENS_ATOMS.append(atom)


def updateProtrusion(distance, density):
    """
    Update protrusions by new distance and density

    :param distance: Int, the distance within a protrusion is satisfied
    :param density: Int, the max number of other CA & CB atoms that is restricting the protrusion
    :return:
    """

    # Subset c-alpha and c-beta atoms
    global PROTEIN
    atoms = PROTEIN.atoms
    rel_atoms = [atom for atom in atoms if atom.name == "CA" or atom.name == "CB"]
    N = len(rel_atoms)

    # Reassign low density atoms on the convex hull
    global LOWDENS_ATOMS, DIST
    LOWDENS_ATOMS = []
    for idx, row in enumerate(DIST):
        atom = rel_atoms[idx]

        # Atom has to be on the convex hull
        if atom in VERTICES:

            # Atom has to be CB
            if atom.name == "CB":

                # Residue has to have low density
                if row[row < distance].shape[0] - 1 < density:
                    LOWDENS_ATOMS.append(atom)


def updateVertexColor(color):
    """
    Update the current vertex color

    :param color: himera.MaterialColor, the color of all protrusions
    :return: None
    """

    # Check color
    if not isinstance(color, chimera.MaterialColor):
        raise ValueError("Illegal color object - has to be chimera.MaterialColor object!")

    # Check if there is any cached vertices to update
    global VERTICES, CACHED_CONV_VERTICES
    if not CACHED_CONV_VERTICES:
        return

    # Update color
    for vertex in CACHED_CONV_VERTICES:
        vertex.color = color


def updateProtrusionColor(color):
    """
    Colorize any convex hull vertex with low atom density (protrusion)

    :param color: chimera.MaterialColor, the color of all protrusions
    :return: None
    """

    # Check color
    if not isinstance(color, chimera.MaterialColor):
        raise ValueError("Illegal color object - has to be chimera.MaterialColor object!")

    # Draw / colorize any atom with low density (protrusion)
    global LOWDENS_ATOMS
    for vertex in LOWDENS_ATOMS:
        vertex.color = color


def updateHydrophobicProtrusionColor(color):
    """
    Colorize any hydrophobic convex hull vertex with low atom density (protrusion)

    :param color: chimera.MaterialColor, the color of all hydrophobic protrusions
    :return: None
    """

    # Check color
    if not isinstance(color, chimera.MaterialColor):
        raise ValueError("Illegal color object - has to be chimera.MaterialColor object!")

    # Draw / colorize any atom with low density (protrusion)
    global LOWDENS_ATOMS, HYDROPHOBICS
    for vertex in LOWDENS_ATOMS:
        if vertex.residue.type in HYDROPHOBICS:
            vertex.color = color


def drawVertices(drawV, drawP, drawPH, vColor, pColor, phColor):
    """
    Draw / colorize all convex hull vertices

    :param drawV: bool, true if vertices are visible
    :param drawP: bool, true if protrusions are visible
    :param drawPH: bool, true if hydrophobic protrusions are visible
    :param vColor: chimera.MaterialColor, the color of all vertices
    :param pColor: chimera.MaterialColor, the color of all vertices that are protrusions
    :param phColor: chimera.MaterialColor, the color of all vertices thar are hydrophobic protrusions
    :return:
    """

    # Check colors
    if not isinstance(vColor, chimera.MaterialColor):
        raise ValueError("Illegal color object - has to be chimera.MaterialColor object!")
    if not isinstance(pColor, chimera.MaterialColor):
        raise ValueError("Illegal color object - has to be chimera.MaterialColor object!")
    if not isinstance(phColor, chimera.MaterialColor):
        raise ValueError("Illegal color object - has to be chimera.MaterialColor object!")

    # Check vertices object
    global VERTICES, LOWDENS_ATOMS, HYDROPHOBICS, CACHED_CONV_VERTICES
    if VERTICES is None:
        return

    # Make sure atoms are drawable in any chimera draw preset
    chimera.runCommand("ribbackbone")

    # Draw vertices
    CACHED_CONV_VERTICES = []
    for vertex in VERTICES:
        isP = vertex in LOWDENS_ATOMS
        isH = vertex.residue.type in HYDROPHOBICS
        vertex.drawMode = chimera.Atom.Ball
        vertex.display = True

        if drawPH and isP and isH:
            vertex.color = phColor
        elif drawP and isP:
            vertex.color = pColor
        elif drawV:
            vertex.color = vColor
            CACHED_CONV_VERTICES.append(vertex)
        else:
            vertex.display = False
