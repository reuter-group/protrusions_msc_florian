# -*- coding: utf-8 -*-

import Tkinter
import tkFont

import chimera
from chimera import dialogs
from chimera.baseDialog import ModelessDialog

import ConvexHull  # Imports plugin code from __init__.py
from CGLtk import Hybrid
from CGLtk.color import ColorWell


class Convexhull_Dialog(ModelessDialog):
    """
    Convex Hull Dialog class

    This class is used to fill in all necessary UI elements for a convex hull dialog and to make use of the user input
    by calling the correct plugin function.
    """

    # @Override
    title = "Convex Hull"

    # @Override
    name = "convex hull"

    # @Override
    buttons = ("Generate", "Close")

    isFilledInUI = False

    def __init__(self, *args, **kw):
        super(Convexhull_Dialog, self).__init__(*args, **kw)

    # @Override
    def fillInUI(self, parent):
        """
        This function fills in all UI elements and generates the main dialog content.

        :param parent: Tkinter.Tk(), the Tkinter window or frame to add the UI to
        :return: None
        """

        # Prepare
        row = 0
        self.masterFrame = Tkinter.Frame(parent)
        self.masterFrame.grid(row=0, column=0, sticky="nesw")

        # Loading title
        text = "\n\tLet me think, please!\t"
        self.loading = Tkinter.Label(parent, text=text)
        self.loading.grid(row=0, column=0, sticky="nesw")
        self.loading.config(font=("TkDefaultFont", 13))

        # Selection title
        label = Tkinter.Label(self.masterFrame, text="\n1) Selection\n")
        label.grid(row=row, column=0, sticky="w")
        label.config(font=("TkDefaultFont", 13))
        f = tkFont.Font(label, label.cget("font"))
        f.configure(underline=True)
        label.configure(font=f)
        row += 1

        # Molecule selection
        self.__molecules = [mol for mol in chimera.openModels.list() if (mol.__class__ == chimera.Molecule)]
        self.__items = ["#" + str(mol.id) + " " + mol.name for mol in self.__molecules]

        Tkinter.Label(self.masterFrame, text="Molecule / Protein:").grid(row=row, column=0, sticky="w")
        row += 1

        dropwdown = Hybrid.Option_Menu(self.masterFrame, "", *self.__items)
        dropwdown.frame.grid(row=row, column=0, sticky="w")
        dropwdown.add_callback(self.changeMolecule_cb)
        self.molecule_selection = dropwdown.variable
        row += 1

        # Atom & Amino acid selection
        frame = Tkinter.Frame(self.masterFrame)
        frame.grid(row=row, column=0, sticky="w")
        row += 1
        frow = 0

        Tkinter.Label(frame, text="\n").grid(row=frow, column=0)
        frow += 1

        Tkinter.Label(frame, text=" Atom type:").grid(row=frow, column=0, sticky="w")
        Tkinter.Label(frame, text="\t Amino acid type:").grid(row=frow, column=1, sticky="w")
        frow += 1

        possible_selection = ["C-Alpha & C-Beta", "C-Beta", "C-Alpha"]
        dropdown = Hybrid.Option_Menu(frame, "", *possible_selection)
        dropdown.frame.grid(row=frow, column=0, sticky="w")
        dropdown.add_callback(self.update_cb)
        self.atom_selection = dropdown.variable

        possible_selection = ["None", "Hydrophobic", "Aromatic", "Positive Charged", "Negative Charged"]
        dropdown = Hybrid.Option_Menu(frame, "\t", *possible_selection)
        dropdown.frame.grid(row=frow, column=1, sticky="w")
        dropdown.add_callback(self.compute_convex_hull)
        self.amino_acid_selection = dropdown.variable

        # Viewport selection checkbox
        self.only_on_viewport_selection = Tkinter.IntVar()
        checkbox = Tkinter.Checkbutton(self.masterFrame, text="only among viewport selection",
                                       variable=self.only_on_viewport_selection,
                                       command=self.compute_convex_hull)
        checkbox.grid(row=row, column=0, sticky="w")
        row += 1

        # Look & feel title
        label = Tkinter.Label(self.masterFrame, text="\n2) Look & Feel\n")
        label.grid(row=row, column=0, sticky="w")
        label.config(font=("TkDefaultFont", 13))
        f = tkFont.Font(label, label.cget("font"))
        f.configure(underline=True)
        label.configure(font=f)
        row += 1

        # Color picker frame
        frame = Tkinter.Frame(self.masterFrame)
        frame.grid(row=row, column=0, sticky="w")

        # Color picker for convex hull
        cf = Tkinter.Frame(frame)
        cf.grid(row=0, column=0, sticky="w")
        row += 1

        Tkinter.Label(cf, text="Convex hull color: ").grid(row=0, column=0, sticky="w")
        initial_color = (0.0, 0.28, 0.6, 0.92)  # nice blue
        cw = ColorWell.ColorWell(cf, callback=self.color_changed_cb)
        cw.showColor(initial_color)
        cw.grid(row=0, column=1, sticky="w")
        self.color = cw
        row += 1

        # Color picker for vertices
        cf2 = Tkinter.Frame(frame)
        cf2.grid(row=0, column=1, sticky="w")
        row += 1

        Tkinter.Label(cf2, text="\tConvex hull vertices color: ").grid(row=0, column=0, sticky="w")
        initial_color2 = (0.3, 0.3, 0.3, 1.0)  # nice gray
        cw2 = ColorWell.ColorWell(cf2, callback=self.update_vertices_color_cb)
        cw2.showColor(initial_color2)
        cw2.grid(row=0, column=1, sticky="w")
        self.vertex_color = cw2
        row += 1

        # Some free space behind color pickers
        Tkinter.Label(frame, text=" \t ").grid(row=0, column=2, sticky="w")

        # Checkbox - Show convex hull vertices
        self.showVertices = Tkinter.IntVar()
        view_vertices = Tkinter.Checkbutton(self.masterFrame, text="Show convex hull vertices",
                                            variable=self.showVertices, command=self.update_vertices_cb)
        view_vertices.grid(row=row, column=0, sticky="w")
        row += 1

        # Size of atoms
        self.atom_size = 1.0
        scale = Hybrid.Scale(self.masterFrame, "Vertex size:\t\t", 0.1, 3, 0.1, self.atom_size)
        scale.frame.grid(row=row, column=0, sticky="we")
        scale.callback(self.change_vertex_size_cb)
        self.size = scale
        self.change_vertex_size_cb()
        row += 1

        # Protrusion title
        label = Tkinter.Label(self.masterFrame, text="\n3) Protrusions\n")
        label.grid(row=row, column=0, sticky="w")
        label.config(font=("TkDefaultFont", 13))
        f = tkFont.Font(label, label.cget("font"))
        f.configure(underline=True)
        label.configure(font=f)
        row += 1

        # Local max distance / radius
        self.max_distance = 10
        scale = Hybrid.Scale(self.masterFrame, "Max local distance (in Å):\t\t", 1, 20, 1, self.max_distance)
        scale.frame.grid(row=row, column=0, sticky="we")
        scale.callback(self.update_vertices_cb)
        self.distance = scale
        row += 1

        # Local max number of C-Alpha's & C-Beta's
        self.max_density = 22
        scale = Hybrid.Scale(self.masterFrame, "Max number of C-Alpha's & C-Beta's:\t", 1, 50, 1, self.max_density)
        scale.frame.grid(row=row, column=0, sticky="we")
        scale.callback(self.update_vertices_cb)
        self.density = scale
        row += 1

        # Color picker & checkbox for protrusions
        cf2 = Tkinter.Frame(self.masterFrame)
        cf2.grid(row=row, column=0, sticky="w")
        row += 1

        self.showProtrusions = Tkinter.IntVar()
        checkbox = Tkinter.Checkbutton(cf2,
                                       text="Mark protrusions",
                                       variable=self.showProtrusions,
                                       command=self.update_vertices_cb)
        checkbox.grid(row=0, column=0, sticky="w")
        initial_color2 = (0.16, 0.48, 0.16, 1.0)  # nice green
        cw2 = ColorWell.ColorWell(cf2, callback=self.update_protrusion_color_cb)
        cw2.showColor(initial_color2)
        cw2.grid(row=0, column=1, sticky="w")
        self.protrusion_color = cw2

        # Color picker & checkbox for hydrophobic protrusions
        cf2 = Tkinter.Frame(self.masterFrame)
        cf2.grid(row=row, column=0, sticky="w")
        row += 1

        self.showHydrophobicProtrusions = Tkinter.IntVar()
        checkbox = Tkinter.Checkbutton(cf2,
                                       text="Mark hydrophobic protrusions",
                                       variable=self.showHydrophobicProtrusions,
                                       command=self.update_vertices_cb)
        checkbox.grid(row=0, column=0, sticky="w")
        initial_color2 = (1.00, 0.40, 0.04, 1.0)  # nice orange
        cw2 = ColorWell.ColorWell(cf2, callback=self.update_hydrophobic_protrusion_color_cb)
        cw2.showColor(initial_color2)
        cw2.grid(row=0, column=1, sticky="w")
        self.hydrophobic_protrusion_color = cw2

        # Some free space at the bottom
        space = Tkinter.Label(self.masterFrame, text="\n")
        space.grid(row=row, column=0, sticky="w")

        # First time generation
        self.isFilledInUI = True
        self.compute_convex_hull()

    def Generate(self):
        """
        Generate & show the convex hull, based on input

        :return: None
        """
        self.compute_convex_hull(temporary=False)
        self.Close()

    def compute_convex_hull(self, temporary=True):
        """
        Generate & show the convex hull, based on input

        :return: None
        """

        if not self.isFilledInUI:
            return

        # Get protein / molecule selection
        if not self.__molecules:
            return
        protein = self.__molecules[self.__items.index(self.molecule_selection.get())]

        # TT modifications
        self.protein = protein  # TODO: optimisation (opt:A)
        # Get atom selection
        atom_selection = self.atom_selection.get()
        if atom_selection == "C-Alpha":
            atom_selection = "CA"
        elif atom_selection == "C-Beta":
            atom_selection = "CB"
        else:
            atom_selection = "both"

        # Get amino acid selection
        amino_selection = self.amino_acid_selection.get()
        if amino_selection == "Hydrophobic":
            amino_selection = "hydrophobic"
        elif amino_selection == "Aromatic":
            amino_selection = "aromatic"
        elif amino_selection == "Positive Charged":
            amino_selection = "charged_positive"
        elif amino_selection == "Negative Charged":
            amino_selection = "charged_negative"
        else:
            amino_selection = None

        # Get among selection value
        among_selection = self.only_on_viewport_selection.get()

        # Get convex hull color
        convHullColor = chimera.MaterialColor(
            self.color.rgba[0],
            self.color.rgba[1],
            self.color.rgba[2],
            self.color.rgba[3]
        )

        # Get vertex color
        vertexColor = chimera.MaterialColor(
            self.vertex_color.rgba[0],
            self.vertex_color.rgba[1],
            self.vertex_color.rgba[2],
            self.vertex_color.rgba[3]
        )

        # Get show selection's
        showVertices = self.showVertices.get()
        showProtrusions = self.showProtrusions.get()
        showHydrophobes = self.showHydrophobicProtrusions.get()

        self.max_distance = self.distance.value(0)
        self.max_density = self.density.value(0)

        # Get protrusion colors
        pColor = chimera.MaterialColor(
            self.protrusion_color.rgba[0],
            self.protrusion_color.rgba[1],
            self.protrusion_color.rgba[2],
            self.protrusion_color.rgba[3]
        )

        phColor = chimera.MaterialColor(
            self.hydrophobic_protrusion_color.rgba[0],
            self.hydrophobic_protrusion_color.rgba[1],
            self.hydrophobic_protrusion_color.rgba[2],
            self.hydrophobic_protrusion_color.rgba[3]
        )

        # Show loading
        loadingBool = len([atom for atom in protein.atoms if atom.name == "CA" or atom.name == "CB"]) > 1000
        if loadingBool and temporary:
            self.loading.grid()

        # Finally, do the magic
        ConvexHull.createConvexHull(protein, atom_selection, amino_selection, among_selection, convHullColor,
                                    vertexColor, showVertices, self.max_distance, self.max_density, showProtrusions,
                                    showHydrophobes, pColor, phColor, temporary)

        # Remove loading again
        self.loading.grid_remove()

    # @Override
    def Close(self):
        """
        Destroy the window correctly if closed. Unfortunately this is not done by Chimera correctly

        :return: None
        """

        # Remove all temporary convex hulls
        try:
            ConvexHull.deleteAllTemporaryConvexHull()

            # Get all proteins & reset atom style
            if self.__molecules:
                for protein in self.__molecules:
                    import _surface as graphics
                    hulls = [hull for hull in chimera.openModels.list() if (hull.__class__ == graphics.SurfaceModel)
                             and "Convex Hull" in hull.name and protein.name in hull.name]
                    if not len(hulls) > 0:
                        ConvexHull.resetAtomStyle(protein)
        except:
            # TODO: lazy fix
            pass

        # Close the dialog correctly
        window = [dialog for dialog in dialogs.activeDialogs() if (dialog.name == "convex hull")][0]
        window.destroy()

    def update_cb(self, event=None):
        """
        Regenerate / update any convex hull computation

        :return: None
        """
        self.compute_convex_hull()

    def changeMolecule_cb(self, event=None):
        """
        Removes all temporary convex hull and recomputes the convex hull on the new molecule that became selected

        :return: None
        """

        # Get protein / molecule selection & reset atom style
        if self.__molecules and not self.showVertices.get():
            for protein in self.__molecules:
                ConvexHull.resetAtomStyle(protein)

        # Remove all temporary computed convex hull objects & recompute convex hull on new molecule
        ConvexHull.deleteAllTemporaryConvexHull()
        self.compute_convex_hull()

    def color_changed_cb(self, event=None):
        """
        Repaint the convex hull on color change

        :param event: ColorWell callback, the color change event
        :return: None
        """

        # Check molecule selection
        if not self.__molecules or not hasattr(self, "color"):
            return

        # Get current protein
        protein = self.__molecules[self.__items.index(self.molecule_selection.get())]

        # Get new convex hull color
        color = chimera.MaterialColor(
            self.color.rgba[0],
            self.color.rgba[1],
            self.color.rgba[2],
            self.color.rgba[3]
        )

        ConvexHull.changeColorOfConvexHull(protein, color)

    def update_vertices_cb(self, event=None):
        """
        Repaint vertices (atoms) on color change

        :param event: ColorWell callback, the color change event
        :return: None
        """

        # Check if attributes are already set
        if not hasattr(self, "vertex_color"):
            return
        if not hasattr(self, "protrusion_color") or not hasattr(self, "hydrophobic_protrusion_color"):
            return

        # Get show vertex selection
        drawV = self.showVertices.get()

        # Get distance & density, recompute protrusion
        self.max_distance = self.distance.value(0)
        self.max_density = self.density.value(0)
        ConvexHull.updateProtrusion(self.max_distance, self.max_density)

        drawP = self.showProtrusions.get()
        drawPH = self.showHydrophobicProtrusions.get()

        # Get colors
        vColor = chimera.MaterialColor(
            self.vertex_color.rgba[0],
            self.vertex_color.rgba[1],
            self.vertex_color.rgba[2],
            self.vertex_color.rgba[3]
        )

        pColor = chimera.MaterialColor(
            self.protrusion_color.rgba[0],
            self.protrusion_color.rgba[1],
            self.protrusion_color.rgba[2],
            self.protrusion_color.rgba[3]
        )

        phColor = chimera.MaterialColor(
            self.hydrophobic_protrusion_color.rgba[0],
            self.hydrophobic_protrusion_color.rgba[1],
            self.hydrophobic_protrusion_color.rgba[2],
            self.hydrophobic_protrusion_color.rgba[3]
        )

        # Update
        ConvexHull.drawVertices(drawV, drawP, drawPH, vColor, pColor, phColor)

    def update_vertices_color_cb(self, event=None):
        """
        Update vertex color
        :return: None
        """

        # Check attributes
        if not hasattr(self, "vertex_color"):
            return

        # Get color
        vColor = chimera.MaterialColor(
            self.vertex_color.rgba[0],
            self.vertex_color.rgba[1],
            self.vertex_color.rgba[2],
            self.vertex_color.rgba[3]
        )

        # Recolorize
        ConvexHull.updateVertexColor(vColor)

    def update_protrusion_color_cb(self, event=None):
        """
        Update protrusion color
        :return: None
        """

        # Check for attributes
        if not hasattr(self, "protrusion_color"):
            return

        # Get color
        pColor = chimera.MaterialColor(
            self.protrusion_color.rgba[0],
            self.protrusion_color.rgba[1],
            self.protrusion_color.rgba[2],
            self.protrusion_color.rgba[3]
        )

        # Recolorize
        ConvexHull.updateProtrusionColor(pColor)

    def update_hydrophobic_protrusion_color_cb(self, event=None):
        """
        Update hydrophobic protrusion color
        :return: None
        """

        # Check for attributes
        if not hasattr(self, "hydrophobic_protrusion_color"):
            return

        # Color
        phColor = chimera.MaterialColor(
            self.hydrophobic_protrusion_color.rgba[0],
            self.hydrophobic_protrusion_color.rgba[1],
            self.hydrophobic_protrusion_color.rgba[2],
            self.hydrophobic_protrusion_color.rgba[3]
        )

        # Recolorize
        ConvexHull.updateHydrophobicProtrusionColor(phColor)

    def change_vertex_size_cb(self, event=None):
        """
        Recompute vertex / atom size
        :return: None
        """

        # Get size
        self.atom_size = self.size.value(0)

        # Redraw
        chimera.runCommand("setattr m ballScale " + str(self.atom_size))


def convexhull_dialog(create=0):
    """
    Find & create the main convex hull dialog instance

    :param create: Bool, if create is true then the dialog constructor will be called on dialog registration.
    :return: The current dialog instance, representing the convex hull dialog
    """
    return dialogs.find(Convexhull_Dialog.name, create=create)


def show_convexhull_dialog():
    """
    Show / display the main convex hull dialog

    :return: The current dialog instance, representing the convex hull dialog
    """
    return dialogs.display(Convexhull_Dialog.name)


# Register the dialog
dialogs.register(Convexhull_Dialog.name, Convexhull_Dialog, replace=1)
