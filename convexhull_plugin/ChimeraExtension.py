import chimera.extension


class Convexhull_EMO(chimera.extension.EMO):
    """
    Main chimera extension class, defining the convex hull plugin
    """

    # @Override
    def name(self):
        return "Convex Hull"

    # @Override
    def description(self):
        s = "Compute a convex hull for a protein, based on C-alpha, C-beta "
        s += "or both"
        return s

    # @Override
    def categories(self):
        return ["Higher-Order Structure"]

    # @Override
    def icon(self):
        return None

    # @Override
    def activate(self):
        self.module("gui").show_convexhull_dialog()
        return None


# Initialize the extension / plugin by default
chimera.extension.manager.registerExtension(Convexhull_EMO(__file__))
